/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVECOMPASS_H
#define WIOGROVECOMPASS_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveCompass : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float compassHeading READ compass_heading NOTIFY compassHeadingChanged)

public:
    WioGroveCompass( QObject *parent = 0);

    float compass_heading() const;

Q_SIGNALS:
    void compassHeadingChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_compass_heading;

};

#endif // WIOGROVECOMPASS_H

