/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "wionode.h"
#include <QQmlEngine>
#include <QQmlContext>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

WioNode::WioNode(QObject *parent) : QObject(parent),
    m_nodeOnline(false),
    m_isValid(false)
{

}

QString WioNode::nodeKey() const
{
    return m_nodeKey;
}

QString WioNode::dataExchangeServer() const
{
    return m_dataExchangeServer;
}

void WioNode::setDataExchangeServer(const QString &dataExchangeServer)
{
    if ( m_dataExchangeServer != dataExchangeServer ) {
        m_dataExchangeServer = dataExchangeServer;
        resolveNodeKey();
        emit dataExchangeServerChanged();
    }
}

QByteArray WioNode::getNodeToken() const
{
    return QByteArray("token ") + m_nodeKey.toLatin1();
}

QByteArray WioNode::getUserToken() const
{
    return QByteArray("token ") + m_userToken.toLatin1();
}

QUrl WioNode::getUrlForServer( const QString &path) const
{
    return serverUrl(QLatin1String("https"),path);
}

void WioNode::setIsValid(bool valid)
{
    if ( m_isValid != valid ) {
        m_isValid = valid;
        emit isValidChanged();
    }
}

void WioNode::setLastError(const QString &err)
{
    if ( m_lastError != err ) {
        m_lastError = err;
        emit lastErrorChanged();
        qDebug() << Q_FUNC_INFO << err;
    }
}

void WioNode::resolveNodeKey( )
{
    if ( m_dataExchangeServer.isEmpty() || m_nodeName.isEmpty() ) {
        setIsValid(false);
        return;
    }

    QQmlContext *context = QQmlEngine::contextForObject(this);
    QNetworkAccessManager *nam = context->engine()->networkAccessManager();
    QNetworkRequest request;
    request.setRawHeader("Authorization", getUserToken());
    request.setUrl(getUrlForServer(QLatin1String("/v1/nodes/list")));
    QNetworkReply *reply = nam->get(request);

    connect( reply, &QNetworkReply::finished, [=]{
        if( reply->error() == QNetworkReply::NoError ) {
            QJsonParseError error;
            QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
            if ( error.error == QJsonParseError::NoError ) {

                QJsonArray nodes = doc.object()[QLatin1String("nodes")].toArray();
                foreach( QJsonValue node, nodes) {
                    if ( node.toObject()[QLatin1String("name")].toString() == m_nodeName ) {
                        setNodeKey(node.toObject()[QLatin1String("node_key")].toString());
                        setNodeSerialNumber(node.toObject()[QLatin1String("node_sn")].toString());
                        setNodeBoardName(node.toObject()[QLatin1String("board")].toString());
                        setNodeOnline(node.toObject()[QLatin1String("online")].toBool());
                        return;
                    }
                }

                setLastError(QString(QLatin1String("Node \"%1\" not found")).arg(m_nodeName));
                setIsValid(false);
            } else {
                setLastError(error.errorString());
                setIsValid(false);
            }
        } else {
            setLastError(reply->errorString());
            setIsValid(false);
        }

        reply->deleteLater();
    });
}

bool WioNode::nodeOnline() const
{
    return m_nodeOnline;
}

void WioNode::setNodeOnline(bool nodeOnline)
{
    if( m_nodeOnline != nodeOnline ) {
        m_nodeOnline = nodeOnline;
        emit nodeOnlineChanged();
    }
}

QString WioNode::nodeSerialNumber() const
{
    return m_nodeSerialNumber;
}

void WioNode::setNodeSerialNumber(const QString &nodeSerialNumber)
{
    if ( m_nodeSerialNumber != nodeSerialNumber ) {
        m_nodeSerialNumber = nodeSerialNumber;
        emit nodeSerialNumberChanged();
    }
}

QString WioNode::nodeBoardName() const
{
    return m_nodeBoardName;
}

void WioNode::setNodeBoardName(const QString &nodeBoardName)
{
    if ( m_nodeBoardName != nodeBoardName ) {
        m_nodeBoardName = nodeBoardName;
        emit nodeBoardNameChanged();
    }
}

QString WioNode::userToken() const
{
    return m_userToken;
}

void WioNode::setUserToken(const QString &token)
{
    if ( m_userToken != token ) {
        m_userToken = token;
        emit userTokenChanged();
        resolveNodeKey();
    }
}

QQmlListProperty<WioGroveModule> WioNode::modules()
{
    return QQmlListProperty<WioGroveModule>(this, m_modules);
}

bool WioNode::isValid() const
{
    return m_isValid;
}

QNetworkRequest WioNode::baseNetworkRequest(const QString &path) const
{
    QNetworkRequest request;
    if ( isValid() ) {
        request.setRawHeader("Authorization", getNodeToken());
        request.setUrl(getUrlForServer(path));
    }

    return request;
}

QUrl WioNode::serverUrl(const QString &protocol, const QString &path) const
{
    QUrl result;
    result.setScheme(protocol);
    result.setHost(m_dataExchangeServer);
    result.setPath(path);

    return result;
}

QString WioNode::lastError() const
{
    return m_lastError;
}

void WioNode::updateBoardInfo()
{
    if ( m_dataExchangeServer.isEmpty() || m_nodeKey.isEmpty() || m_userToken.isEmpty() ) {
        setIsValid(false);
        return;
    }

    QQmlContext *context = QQmlEngine::contextForObject(this);
    QNetworkAccessManager *nam = context->engine()->networkAccessManager();
    QNetworkRequest request;
    request.setRawHeader("Authorization", getNodeToken());
    request.setUrl(getUrlForServer(QLatin1String("/v1/node/.well-known")));
    QNetworkReply *reply = nam->get(request);

    connect( reply, &QNetworkReply::finished, [=]{
        if( reply->error() == QNetworkReply::NoError ) {
            QJsonParseError error;
            QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
            if ( error.error == QJsonParseError::NoError ) {
                setLastError(QString());
                setIsValid(true);

            } else {
                setLastError(error.errorString());
                setIsValid(false);
            }
        } else {
            setLastError(reply->errorString());
            setIsValid(false);
        }

        reply->deleteLater();
    });

}

void WioNode::sleep(const quint32 &seconds)
{
    if ( isValid() ) {
        return;
    }

    if ( m_dataExchangeServer.isEmpty() || m_nodeKey.isEmpty() ) {
        setIsValid(false);
        return;
    }

    QQmlContext *context = QQmlEngine::contextForObject(this);
    QNetworkAccessManager *nam = context->engine()->networkAccessManager();
    QNetworkRequest request;
    request.setRawHeader("Authorization", getNodeToken());
    request.setUrl(getUrlForServer(QString(QLatin1String("/v1/node/pm/sleep/%1")).arg(seconds)));
    QNetworkReply *reply = nam->post(request,QByteArray());

    connect( reply, &QNetworkReply::finished, [&]{
        if( reply->error() == QNetworkReply::NoError ) {
            QJsonParseError error;
            QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
            if ( error.error == QJsonParseError::NoError ) {

                if (doc.object()[QLatin1String("result")].toString() != QLatin1String("OK")) {
                    setLastError(doc.object()[QLatin1String("result")].toString());
                } else {
                    setLastError(QString());
                    emit sleepSuccessful();
                }
            } else {
                setLastError(error.errorString());
                setIsValid(false);
            }
        } else {
            setLastError(reply->errorString());
            setIsValid(false);
        }

        reply->deleteLater();
    });

}

void WioNode::update()
{
    resolveNodeKey();
}

QString WioNode::nodeName() const
{
    return m_nodeName;
}

void WioNode::setNodeName(const QString &name)
{
    if ( m_nodeName != name ) {
        m_nodeName = name;
        resolveNodeKey();
        emit nodeNameChanged();
    }
}

void WioNode::setNodeKey(const QString &key)
{
    if ( m_nodeKey != key ) {
        m_nodeKey = key;
        updateBoardInfo();
        emit nodeKeyChanged();
    }
}

void WioNode::classBegin()
{

}

void WioNode::componentComplete()
{
    resolveNodeKey();
}
