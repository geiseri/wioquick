/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveaccmma7660.h"
#include <QJsonObject>

WioGroveAccMMA7660::WioGroveAccMMA7660( QObject *parent) : WioGroveModule(parent)
{
     
     m_shaked = 0;

}

void WioGroveAccMMA7660::update()
{

    readProperty( QLatin1String("acceleration"), [&](const QJsonObject &object){
       m_acceleration[QLatin1String("ax")] = object.toVariantMap()[QLatin1String("ax")].value<float>();
       m_acceleration[QLatin1String("ay")] = object.toVariantMap()[QLatin1String("ay")].value<float>();
       m_acceleration[QLatin1String("az")] = object.toVariantMap()[QLatin1String("az")].value<float>();

       Q_EMIT accelerationChanged();
    });
    readProperty( QLatin1String("shaked"), [&](const QJsonObject &object){
       m_shaked = object.toVariantMap()[QLatin1String("shaked")].value<quint8>();

       Q_EMIT shakedChanged();
    });
}

QVariantMap WioGroveAccMMA7660::acceleration() const
{
    return m_acceleration;
}

quint8 WioGroveAccMMA7660::shaked() const
{
    return m_shaked;
}




QString WioGroveAccMMA7660::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveAccMMA7660%1")).arg(pin);
}

