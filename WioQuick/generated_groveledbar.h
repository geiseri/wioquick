/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVELEDBAR_H
#define WIOGROVELEDBAR_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveLEDBar : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(quint16 bits READ bits WRITE set_bits NOTIFY bitsChanged)

public:
    WioGroveLEDBar( QObject *parent = 0);

    quint16 bits() const;
    void set_bits( const quint16 &val );

Q_SIGNALS:
    void bitsChanged();


public Q_SLOTS:
    virtual void update();
    void callLevel(const float &level);
    void callOrientation(const quint8 &green_to_red);
    void callSingleLed(const quint8 &led,const float &brightness);
    void callToggle(const quint8 &led);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    quint16 m_bits;

};

#endif // WIOGROVELEDBAR_H

