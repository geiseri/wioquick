/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovehall.h"
#include <QJsonObject>

WioGroveHall::WioGroveHall( QObject *parent) : WioGroveModule(parent)
{
     m_vout = 0;

}

void WioGroveHall::update()
{

    readProperty( QLatin1String("vout"), [&](const QJsonObject &object){
       m_vout = object.toVariantMap()[QLatin1String("vout")].value<quint8>();

       Q_EMIT voutChanged();
    });
}

quint8 WioGroveHall::vout() const
{
    return m_vout;
}




QString WioGroveHall::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveHall%1")).arg(pin);
}

bool WioGroveHall::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("state_off") ) {
        Q_EMIT eventStateOff(data.value<qint16>());
        return true;
    }
    if ( event == QLatin1String("state_on") ) {
        Q_EMIT eventStateOn(data.value<qint16>());
        return true;
    }
    return false;
}

bool WioGroveHall::hasEvents( ) const
{
    return true;
}
