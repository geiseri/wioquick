/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVESI114X_H
#define WIOGROVESI114X_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveSI114X : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float ir READ IR NOTIFY irChanged)
    Q_PROPERTY(float uv READ UV NOTIFY uvChanged)
    Q_PROPERTY(float visiblelight READ visiblelight NOTIFY visiblelightChanged)

public:
    WioGroveSI114X( QObject *parent = 0);

    float IR() const;
    float UV() const;
    float visiblelight() const;

Q_SIGNALS:
    void irChanged();
    void uvChanged();
    void visiblelightChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_IR;
    float m_UV;
    float m_visiblelight;

};

#endif // WIOGROVESI114X_H

