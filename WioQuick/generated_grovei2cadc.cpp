/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovei2cadc.h"
#include <QJsonObject>

WioGroveI2CAdc::WioGroveI2CAdc( QObject *parent) : WioGroveModule(parent)
{
     m_adc = 0;
     m_voltage = 0;

}

void WioGroveI2CAdc::update()
{

    readProperty( QLatin1String("adc"), [&](const QJsonObject &object){
       m_adc = object.toVariantMap()[QLatin1String("adc_reading")].value<quint32>();

       Q_EMIT adcChanged();
    });
    readProperty( QLatin1String("voltage"), [&](const QJsonObject &object){
       m_voltage = object.toVariantMap()[QLatin1String("volt")].value<float>();

       Q_EMIT voltageChanged();
    });
}

quint32 WioGroveI2CAdc::adc() const
{
    return m_adc;
}

float WioGroveI2CAdc::voltage() const
{
    return m_voltage;
}




QString WioGroveI2CAdc::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveI2CAdc%1")).arg(pin);
}

