/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEMOISTURE_H
#define WIOGROVEMOISTURE_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveMoisture : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(quint16 moisture READ moisture NOTIFY moistureChanged)

public:
    WioGroveMoisture( QObject *parent = 0);

    quint16 moisture() const;

Q_SIGNALS:
    void moistureChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    quint16 m_moisture;

};

#endif // WIOGROVEMOISTURE_H

