/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovetemphum.h"
#include <QJsonObject>

WioGroveTempHum::WioGroveTempHum( QObject *parent) : WioGroveModule(parent)
{
     m_humidity = 0;
     m_temperature = 0;
     m_temperature_f = 0;

}

void WioGroveTempHum::update()
{

    readProperty( QLatin1String("humidity"), [&](const QJsonObject &object){
       m_humidity = object.toVariantMap()[QLatin1String("humidity")].value<float>();

       Q_EMIT humidityChanged();
    });
    readProperty( QLatin1String("temperature"), [&](const QJsonObject &object){
       m_temperature = object.toVariantMap()[QLatin1String("celsius_degree")].value<float>();

       Q_EMIT temperatureChanged();
    });
    readProperty( QLatin1String("temperature_f"), [&](const QJsonObject &object){
       m_temperature_f = object.toVariantMap()[QLatin1String("fahrenheit_degree")].value<float>();

       Q_EMIT temperatureFChanged();
    });
}

float WioGroveTempHum::humidity() const
{
    return m_humidity;
}

float WioGroveTempHum::temperature() const
{
    return m_temperature;
}

float WioGroveTempHum::temperature_f() const
{
    return m_temperature_f;
}




QString WioGroveTempHum::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveTempHum%1")).arg(pin);
}

