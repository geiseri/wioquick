/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_genericain.h"
#include <QJsonObject>

WioGenericAIn::WioGenericAIn( QObject *parent) : WioGroveModule(parent)
{
     m_analog = 0;
     m_voltage = 0;

}

void WioGenericAIn::update()
{

    readProperty( QLatin1String("analog"), [&](const QJsonObject &object){
       m_analog = object.toVariantMap()[QLatin1String("analog")].value<int>();

       Q_EMIT analogChanged();
    });
    readProperty( QLatin1String("voltage"), [&](const QJsonObject &object){
       m_voltage = object.toVariantMap()[QLatin1String("volt")].value<float>();

       Q_EMIT voltageChanged();
    });
}

int WioGenericAIn::analog() const
{
    return m_analog;
}

float WioGenericAIn::voltage() const
{
    return m_voltage;
}




QString WioGenericAIn::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GenericAIn%1")).arg(pin);
}

