/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVESPEAKER_H
#define WIOGROVESPEAKER_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveSpeaker : public WioGroveModule
{
    Q_OBJECT


public:
    WioGroveSpeaker( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callSoundMs(const int &freq,const int &duration_ms);
    void callSoundStart(const int &freq);
    void callSoundStop();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVESPEAKER_H

