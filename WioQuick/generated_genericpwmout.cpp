/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_genericpwmout.h"
#include <QJsonObject>

WioGenericPWMOut::WioGenericPWMOut( QObject *parent) : WioGroveModule(parent)
{
     

}

void WioGenericPWMOut::update()
{

    readProperty( QLatin1String("pwm"), [&](const QJsonObject &object){
       m_pwm[QLatin1String("duty_percent")] = object.toVariantMap()[QLatin1String("duty_percent")].value<float>();
       m_pwm[QLatin1String("freq")] = object.toVariantMap()[QLatin1String("freq")].value<quint32>();

       Q_EMIT pwmChanged();
    });
}


QVariantMap WioGenericPWMOut::pwm() const
{
    return m_pwm;
}

void WioGenericPWMOut::set_pwm( const QVariantMap &val )
{

    if ( m_pwm != val ) {
        writeProperty( QLatin1String("pwm"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val[QLatin1String("duty_percent")],val[QLatin1String("freq")]);
    }

}


void WioGenericPWMOut::callPwmWithFreq(const float &duty_percent,const quint32 &freq)
{
    callAction( QLatin1String("pwm_with_freq"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },duty_percent,freq);

}

QString WioGenericPWMOut::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GenericPWMOut%1")).arg(pin);
}

