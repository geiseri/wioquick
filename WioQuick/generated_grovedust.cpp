/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovedust.h"
#include <QJsonObject>

WioGroveDust::WioGroveDust( QObject *parent) : WioGroveModule(parent)
{
     m_dust = 0;

}

void WioGroveDust::update()
{

    readProperty( QLatin1String("dust"), [&](const QJsonObject &object){
       m_dust = object.toVariantMap()[QLatin1String("dust")].value<float>();

       Q_EMIT dustChanged();
    });
}

float WioGroveDust::dust() const
{
    return m_dust;
}




QString WioGroveDust::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveDust%1")).arg(pin);
}

