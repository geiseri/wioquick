/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovesi114x.h"
#include <QJsonObject>

WioGroveSI114X::WioGroveSI114X( QObject *parent) : WioGroveModule(parent)
{
     m_IR = 0;
     m_UV = 0;
     m_visiblelight = 0;

}

void WioGroveSI114X::update()
{

    readProperty( QLatin1String("IR"), [&](const QJsonObject &object){
       m_IR = object.toVariantMap()[QLatin1String("IR")].value<float>();

       Q_EMIT irChanged();
    });
    readProperty( QLatin1String("UV"), [&](const QJsonObject &object){
       m_UV = object.toVariantMap()[QLatin1String("UV")].value<float>();

       Q_EMIT uvChanged();
    });
    readProperty( QLatin1String("visiblelight"), [&](const QJsonObject &object){
       m_visiblelight = object.toVariantMap()[QLatin1String("VL")].value<float>();

       Q_EMIT visiblelightChanged();
    });
}

float WioGroveSI114X::IR() const
{
    return m_IR;
}

float WioGroveSI114X::UV() const
{
    return m_UV;
}

float WioGroveSI114X::visiblelight() const
{
    return m_visiblelight;
}




QString WioGroveSI114X::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveSI114X%1")).arg(pin);
}

