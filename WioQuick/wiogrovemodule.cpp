/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "wiogrovemodule.h"
#include "wionode.h"
#include <QQmlContext>
#include <QQmlEngine>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QtWebSockets>
#include <QDebug>

WioGroveModule::WioGroveModule(QObject *parent) : QObject(parent)
{
    m_webSocket = new QWebSocket(QString(), QWebSocketProtocol::VersionLatest, this);
    connect( m_webSocket, &QWebSocket::connected, this, &WioGroveModule::onOpenWebSocket);
    connect( m_webSocket, &QWebSocket::textMessageReceived, this, &WioGroveModule::onWebSocketMessage);
    connect( m_webSocket, static_cast<void(QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error),this, &WioGroveModule::onWebSocketError);
}


void WioGroveModule::classBegin()
{
}

void WioGroveModule::componentComplete()
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode ) {
        // connect up isvalid changed to update
        connect( parentNode, &WioNode::isValidChanged, [=]{
            if( parentNode->isValid() ) {
                update();
                connectUpEventHandler();
            }
        });
        if ( parentNode->isValid() ) {
            update();
            connectUpEventHandler();
        }
    }
}

QString WioGroveModule::sensorName() const
{
    return buildSensorName(m_pin);
}

QString joinPath( const QString &instanceName,
                  const QString &slot,
                  const QVariant &arg1,
                  const QVariant &arg2,
                  const QVariant &arg3,
                  const QVariant &arg4)
{
    QStringList parts({instanceName, slot});

    if ( arg1.isValid() ) {
        parts << arg1.toString();
    }

    if ( arg2.isValid() ) {
        parts << arg2.toString();
    }

    if ( arg3.isValid() ) {
        parts << arg3.toString();
    }

    if ( arg4.isValid() ) {
        parts << arg4.toString();
    }


    return QString(QLatin1String("/v1/node/")) + parts.join('/');
}

void WioGroveModule::readProperty(const QString &property, std::function<void(QJsonObject)> functor, const QVariant &arg1, const QVariant &arg2, const QVariant &arg3, const QVariant &arg4)
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode ) {
        QNetworkRequest request = parentNode->baseNetworkRequest(joinPath(sensorName(),property, arg1, arg2, arg3, arg4));
        QQmlContext *context = QQmlEngine::contextForObject(this);
        QNetworkAccessManager *nam = context->engine()->networkAccessManager();

        QNetworkReply *reply = nam->get(request);

        connect( reply, &QNetworkReply::finished, [=]{
            if( reply->error() == QNetworkReply::NoError ) {
                QJsonParseError error;
                QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
                if ( error.error == QJsonParseError::NoError ) {

                    functor(doc.object());

                } else {
                    setLastError(error.errorString());
                }
            } else {
                setLastError(reply->errorString());
            }

            reply->deleteLater();
        });
    }
}

void WioGroveModule::writeProperty(const QString &property, std::function<void(QJsonObject)> functor, const QVariant &arg1, const QVariant &arg2, const QVariant &arg3, const QVariant &arg4)
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode ) {
        QNetworkRequest request = parentNode->baseNetworkRequest(joinPath(sensorName(),property, arg1, arg2, arg3, arg4));
        QQmlContext *context = QQmlEngine::contextForObject(this);
        QNetworkAccessManager *nam = context->engine()->networkAccessManager();

        QNetworkReply *reply = nam->post(request,QByteArray());

        connect( reply, &QNetworkReply::finished, [=]{
            if( reply->error() == QNetworkReply::NoError ) {
                QJsonParseError error;
                QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
                if ( error.error == QJsonParseError::NoError ) {

                    functor(doc.object());

                } else {
                    setLastError(error.errorString());
                }
            } else {
                setLastError(reply->errorString());
            }

            reply->deleteLater();
        });
    }
}

void WioGroveModule::callAction(const QString &action, std::function<void(QJsonObject)> functor, const QVariant &arg1, const QVariant &arg2, const QVariant &arg3, const QVariant &arg4)
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode ) {
        QNetworkRequest request = parentNode->baseNetworkRequest(joinPath(sensorName(), action, arg1, arg2, arg3, arg4));
        QQmlContext *context = QQmlEngine::contextForObject(this);
        QNetworkAccessManager *nam = context->engine()->networkAccessManager();

        QNetworkReply *reply = nam->post(request,QByteArray());

        connect( reply, &QNetworkReply::finished, [=]{
            if( reply->error() == QNetworkReply::NoError ) {
                QJsonParseError error;
                QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error);
                if ( error.error == QJsonParseError::NoError ) {

                    functor(doc.object());

                } else {
                    setLastError(error.errorString());
                }
            } else {
                setLastError(reply->errorString());
            }

            reply->deleteLater();
        });
    }

}

bool WioGroveModule::checkError(const QJsonObject &obj)
{
    if ( obj[QLatin1String("result")].toString() != QLatin1String("OK") ) {
        setLastError(obj[QLatin1String("error")].toString());
        return true;
    } else {
        setLastError(QString());
        return false;
    }
}

QString WioGroveModule::buildSensorName(const QString &pin) const
{
    Q_UNUSED(pin);
    return QString();
}

bool WioGroveModule::hasEvents() const
{
    return false;
}

bool WioGroveModule::emitEvent(const QString &event, const QVariant &data)
{
    Q_UNUSED(event);
    Q_UNUSED(data);
    return false;
}

void WioGroveModule::onOpenWebSocket()
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode ) {
        m_webSocket->sendTextMessage(parentNode->nodeKey());
    }
}

void WioGroveModule::onWebSocketError(const QAbstractSocket::SocketError &error)
{
    Q_UNUSED(error);
    setLastError(m_webSocket->errorString());
}

void WioGroveModule::onWebSocketMessage(const QString &msg)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8(), &error);
    if( error.error == QJsonParseError::NoError ) {
        QJsonObject msg = doc.object()[QLatin1String("msg")].toObject();
        foreach( QString key, msg.keys() ) {
            emitEvent(key,msg[key].toVariant());
        }
    } else {
        setLastError(error.errorString());
    }
}

void WioGroveModule::setLastError(const QString &err)
{
    if ( m_lastError != err ) {
        m_lastError = err;
        emit lastErrorChanged();
        qDebug() << Q_FUNC_INFO << err;
    }
}

void WioGroveModule::connectUpEventHandler()
{
    WioNode *parentNode = qobject_cast<WioNode*>(parent());
    if ( parentNode && hasEvents() && !m_webSocket->isValid() ) {
        QQmlContext *context = QQmlEngine::contextForObject(this);
        QNetworkAccessManager *nam = context->engine()->networkAccessManager();
        m_webSocket->setProxy(nam->proxy());
        m_webSocket->open(parentNode->serverUrl(QLatin1String("wss"), QLatin1String("/v1/node/event")));
    }
}

QString WioGroveModule::pin() const
{
    return m_pin;
}

void WioGroveModule::setPin(const QString &pin)
{
    if ( m_pin != pin ) {
        m_pin = pin;
        emit pinChanged();
        update();
        connectUpEventHandler();
    }
}

QString WioGroveModule::lastError() const
{
    return m_lastError;
}

void WioGroveModule::update()
{

}
