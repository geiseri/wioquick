/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveluminance.h"
#include <QJsonObject>

WioGroveLuminance::WioGroveLuminance( QObject *parent) : WioGroveModule(parent)
{
     m_luminance = 0;

}

void WioGroveLuminance::update()
{

    readProperty( QLatin1String("luminance"), [&](const QJsonObject &object){
       m_luminance = object.toVariantMap()[QLatin1String("lux")].value<float>();

       Q_EMIT luminanceChanged();
    });
}

float WioGroveLuminance::luminance() const
{
    return m_luminance;
}




QString WioGroveLuminance::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveLuminance%1")).arg(pin);
}

