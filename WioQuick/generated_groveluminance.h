/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVELUMINANCE_H
#define WIOGROVELUMINANCE_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveLuminance : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float luminance READ luminance NOTIFY luminanceChanged)

public:
    WioGroveLuminance( QObject *parent = 0);

    float luminance() const;

Q_SIGNALS:
    void luminanceChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_luminance;

};

#endif // WIOGROVELUMINANCE_H

