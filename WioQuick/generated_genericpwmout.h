/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGENERICPWMOUT_H
#define WIOGENERICPWMOUT_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGenericPWMOut : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap pwm READ pwm WRITE set_pwm NOTIFY pwmChanged)

public:
    WioGenericPWMOut( QObject *parent = 0);

    QVariantMap pwm() const;
    void set_pwm( const QVariantMap &val );

Q_SIGNALS:
    void pwmChanged();


public Q_SLOTS:
    virtual void update();
    void callPwmWithFreq(const float &duty_percent,const quint32 &freq);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    QVariantMap m_pwm;

};

#endif // WIOGENERICPWMOUT_H

