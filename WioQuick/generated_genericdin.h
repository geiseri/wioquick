/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGENERICDIN_H
#define WIOGENERICDIN_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGenericDIn : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(quint32 edgeFallSinceLastRead READ edge_fall_since_last_read NOTIFY edgeFallSinceLastReadChanged)
    Q_PROPERTY(quint32 edgeRiseSinceLastRead READ edge_rise_since_last_read NOTIFY edgeRiseSinceLastReadChanged)
    Q_PROPERTY(quint8 input READ input NOTIFY inputChanged)

public:
    WioGenericDIn( QObject *parent = 0);

    quint32 edge_fall_since_last_read() const;
    quint32 edge_rise_since_last_read() const;
    quint8 input() const;

Q_SIGNALS:
    void edgeFallSinceLastReadChanged();
    void edgeRiseSinceLastReadChanged();
    void inputChanged();

    void eventInputChanged(const qint16 &value);
    void eventInputFall(const qint16 &value);
    void eventInputRise(const qint16 &value);

public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
    virtual bool emitEvent( const QString &event, const QVariant &data );
    virtual bool hasEvents() const;
private:
    quint32 m_edge_fall_since_last_read;
    quint32 m_edge_rise_since_last_read;
    quint8 m_input;

};

#endif // WIOGENERICDIN_H

