/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovegyroitg3200.h"
#include <QJsonObject>

WioGroveGyroITG3200::WioGroveGyroITG3200( QObject *parent) : WioGroveModule(parent)
{
     
     m_temperature = 0;

}

void WioGroveGyroITG3200::update()
{

    readProperty( QLatin1String("gyro"), [&](const QJsonObject &object){
       m_gyro[QLatin1String("gx")] = object.toVariantMap()[QLatin1String("gx")].value<float>();
       m_gyro[QLatin1String("gy")] = object.toVariantMap()[QLatin1String("gy")].value<float>();
       m_gyro[QLatin1String("gz")] = object.toVariantMap()[QLatin1String("gz")].value<float>();

       Q_EMIT gyroChanged();
    });
    readProperty( QLatin1String("temperature"), [&](const QJsonObject &object){
       m_temperature = object.toVariantMap()[QLatin1String("temp")].value<float>();

       Q_EMIT temperatureChanged();
    });
}

QVariantMap WioGroveGyroITG3200::gyro() const
{
    return m_gyro;
}

float WioGroveGyroITG3200::temperature() const
{
    return m_temperature;
}



void WioGroveGyroITG3200::callZerocalibrate()
{
    callAction( QLatin1String("zerocalibrate"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}

QString WioGroveGyroITG3200::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveGyroITG3200%1")).arg(pin);
}

