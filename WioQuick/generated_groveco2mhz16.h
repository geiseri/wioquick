/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVECO2MHZ16_H
#define WIOGROVECO2MHZ16_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveCo2MhZ16 : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float concentration READ concentration NOTIFY concentrationChanged)
    Q_PROPERTY(QVariantMap concentrationAndTemperature READ concentration_and_temperature NOTIFY concentrationAndTemperatureChanged)
    Q_PROPERTY(float temperature READ temperature NOTIFY temperatureChanged)

public:
    WioGroveCo2MhZ16( QObject *parent = 0);

    float concentration() const;
    QVariantMap concentration_and_temperature() const;
    float temperature() const;

Q_SIGNALS:
    void concentrationChanged();
    void concentrationAndTemperatureChanged();
    void temperatureChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_concentration;
    QVariantMap m_concentration_and_temperature;
    float m_temperature;

};

#endif // WIOGROVECO2MHZ16_H

