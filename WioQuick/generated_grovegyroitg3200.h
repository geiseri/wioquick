/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEGYROITG3200_H
#define WIOGROVEGYROITG3200_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveGyroITG3200 : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap gyro READ gyro NOTIFY gyroChanged)
    Q_PROPERTY(float temperature READ temperature NOTIFY temperatureChanged)

public:
    WioGroveGyroITG3200( QObject *parent = 0);

    QVariantMap gyro() const;
    float temperature() const;

Q_SIGNALS:
    void gyroChanged();
    void temperatureChanged();


public Q_SLOTS:
    virtual void update();
    void callZerocalibrate();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    QVariantMap m_gyro;
    float m_temperature;

};

#endif // WIOGROVEGYROITG3200_H

