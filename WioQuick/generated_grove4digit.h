/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVE4DIGIT_H
#define WIOGROVE4DIGIT_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGrove4Digit : public WioGroveModule
{
    Q_OBJECT


public:
    WioGrove4Digit( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callBrightness(const quint8 &brightness);
    void callClear();
    void callDisplayDigits(const quint8 &start_pos,const QString &chars);
    void callDisplayOneDigit(const quint8 &position,const QString &chr);
    void callDisplayPoint(const quint8 &display);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVE4DIGIT_H

