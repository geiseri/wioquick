/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEUV_H
#define WIOGROVEUV_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveUV : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float uvIndex READ uv_index NOTIFY uvIndexChanged)

public:
    WioGroveUV( QObject *parent = 0);

    float uv_index() const;

Q_SIGNALS:
    void uvIndexChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_uv_index;

};

#endif // WIOGROVEUV_H

