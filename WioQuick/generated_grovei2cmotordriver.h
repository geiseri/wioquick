/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEI2CMOTORDRIVER_H
#define WIOGROVEI2CMOTORDRIVER_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveI2CMotorDriver : public WioGroveModule
{
    Q_OBJECT


public:
    WioGroveI2CMotorDriver( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callDcmotor1Break();
    void callDcmotor1ChangeDirection();
    void callDcmotor1Resume();
    void callDcmotor2Break();
    void callDcmotor2ChangeDirection();
    void callDcmotor2Resume();
    void callDcmotorSpeed(const quint8 &speed_m1,const quint8 &speed_m2);
    void callDisableStepperMode();
    void callEnableStepperMode(const quint8 &direction,const quint8 &speed);
    void callI2CAddress(const quint8 &addr_7bits);
    void callStepperSteps(const quint8 &steps);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVEI2CMOTORDRIVER_H

