/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovesolidstaterelay.h"
#include <QJsonObject>

WioGroveSolidStateRelay::WioGroveSolidStateRelay( QObject *parent) : WioGroveModule(parent)
{
     m_onoff_status = 0;

}

void WioGroveSolidStateRelay::update()
{

    readProperty( QLatin1String("onoff_status"), [&](const QJsonObject &object){
       m_onoff_status = object.toVariantMap()[QLatin1String("onoff")].value<int>();

       Q_EMIT onoffStatusChanged();
    });
}

int WioGroveSolidStateRelay::onoff_status() const
{
    return m_onoff_status;
}



void WioGroveSolidStateRelay::callOnoff(const int &onoff)
{
    callAction( QLatin1String("onoff"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },onoff);

}

QString WioGroveSolidStateRelay::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveSolidStateRelay%1")).arg(pin);
}

