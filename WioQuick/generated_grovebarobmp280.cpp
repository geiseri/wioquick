/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovebarobmp280.h"
#include <QJsonObject>

WioGroveBaroBMP280::WioGroveBaroBMP280( QObject *parent) : WioGroveModule(parent)
{
     m_altitude = 0;
     m_pressure = 0;
     m_temperature = 0;

}

void WioGroveBaroBMP280::update()
{

    readProperty( QLatin1String("altitude"), [&](const QJsonObject &object){
       m_altitude = object.toVariantMap()[QLatin1String("altitude")].value<float>();

       Q_EMIT altitudeChanged();
    });
    readProperty( QLatin1String("pressure"), [&](const QJsonObject &object){
       m_pressure = object.toVariantMap()[QLatin1String("pressure")].value<qint32>();

       Q_EMIT pressureChanged();
    });
    readProperty( QLatin1String("temperature"), [&](const QJsonObject &object){
       m_temperature = object.toVariantMap()[QLatin1String("temperature")].value<float>();

       Q_EMIT temperatureChanged();
    });
}

float WioGroveBaroBMP280::altitude() const
{
    return m_altitude;
}

qint32 WioGroveBaroBMP280::pressure() const
{
    return m_pressure;
}

float WioGroveBaroBMP280::temperature() const
{
    return m_temperature;
}




QString WioGroveBaroBMP280::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveBaroBMP280%1")).arg(pin);
}

