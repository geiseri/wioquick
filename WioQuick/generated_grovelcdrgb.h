/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVELCDRGB_H
#define WIOGROVELCDRGB_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveLCDRGB : public WioGroveModule
{
    Q_OBJECT


public:
    WioGroveLCDRGB( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callBacklightColor(const quint8 &color_index);
    void callBacklightColorRgb(const quint8 &r,const quint8 &g,const quint8 &b);
    void callBase64String(const quint8 &row,const quint8 &col,const QString &b64_str);
    void callClear();
    void callDisplayOff();
    void callDisplayOn();
    void callFloat(const quint8 &row,const quint8 &col,const float &f,const quint8 &decimal);
    void callInteger(const quint8 &row,const quint8 &col,const qint32 &i);
    void callScrollLeft(const quint8 &speed);
    void callScrollRight(const quint8 &speed);
    void callStopScroll();
    void callString(const quint8 &row,const quint8 &col,const QString &str);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVELCDRGB_H

