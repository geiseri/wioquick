/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveledbar.h"
#include <QJsonObject>

WioGroveLEDBar::WioGroveLEDBar( QObject *parent) : WioGroveModule(parent)
{
     m_bits = 0;

}

void WioGroveLEDBar::update()
{

    readProperty( QLatin1String("bits"), [&](const QJsonObject &object){
       m_bits = object.toVariantMap()[QLatin1String("bits")].value<quint16>();

       Q_EMIT bitsChanged();
    });
}


quint16 WioGroveLEDBar::bits() const
{
    return m_bits;
}

void WioGroveLEDBar::set_bits( const quint16 &val )
{

    if ( m_bits != val ) {
        writeProperty( QLatin1String("bits"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}


void WioGroveLEDBar::callLevel(const float &level)
{
    callAction( QLatin1String("level"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },level);

}
void WioGroveLEDBar::callOrientation(const quint8 &green_to_red)
{
    callAction( QLatin1String("orientation"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },green_to_red);

}
void WioGroveLEDBar::callSingleLed(const quint8 &led,const float &brightness)
{
    callAction( QLatin1String("single_led"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },led,brightness);

}
void WioGroveLEDBar::callToggle(const quint8 &led)
{
    callAction( QLatin1String("toggle"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },led);

}

QString WioGroveLEDBar::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveLEDBar%1")).arg(pin);
}

