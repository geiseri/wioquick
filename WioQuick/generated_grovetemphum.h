/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVETEMPHUM_H
#define WIOGROVETEMPHUM_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveTempHum : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float humidity READ humidity NOTIFY humidityChanged)
    Q_PROPERTY(float temperature READ temperature NOTIFY temperatureChanged)
    Q_PROPERTY(float temperatureF READ temperature_f NOTIFY temperatureFChanged)

public:
    WioGroveTempHum( QObject *parent = 0);

    float humidity() const;
    float temperature() const;
    float temperature_f() const;

Q_SIGNALS:
    void humidityChanged();
    void temperatureChanged();
    void temperatureFChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_humidity;
    float m_temperature;
    float m_temperature_f;

};

#endif // WIOGROVETEMPHUM_H

