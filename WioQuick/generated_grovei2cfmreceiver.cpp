/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovei2cfmreceiver.h"
#include <QJsonObject>

WioGroveI2cFmReceiver::WioGroveI2cFmReceiver( QObject *parent) : WioGroveModule(parent)
{
     m_mute_status = 0;
     m_signal_level = 0;
     m_frequency = 0;
     m_volume = 0;

}

void WioGroveI2cFmReceiver::update()
{

    readProperty( QLatin1String("mute_status"), [&](const QJsonObject &object){
       m_mute_status = object.toVariantMap()[QLatin1String("muted")].value<bool>();

       Q_EMIT muteStatusChanged();
    });
    readProperty( QLatin1String("signal_level"), [&](const QJsonObject &object){
       m_signal_level = object.toVariantMap()[QLatin1String("rssi")].value<quint8>();

       Q_EMIT signalLevelChanged();
    });
    readProperty( QLatin1String("frequency"), [&](const QJsonObject &object){
       m_frequency = object.toVariantMap()[QLatin1String("frequency")].value<quint16>();

       Q_EMIT frequencyChanged();
    });
    readProperty( QLatin1String("volume"), [&](const QJsonObject &object){
       m_volume = object.toVariantMap()[QLatin1String("level")].value<quint8>();

       Q_EMIT volumeChanged();
    });
}

bool WioGroveI2cFmReceiver::mute_status() const
{
    return m_mute_status;
}

quint8 WioGroveI2cFmReceiver::signal_level() const
{
    return m_signal_level;
}


quint16 WioGroveI2cFmReceiver::frequency() const
{
    return m_frequency;
}

void WioGroveI2cFmReceiver::set_frequency( const quint16 &val )
{

    if ( m_frequency != val ) {
        writeProperty( QLatin1String("frequency"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}

quint8 WioGroveI2cFmReceiver::volume() const
{
    return m_volume;
}

void WioGroveI2cFmReceiver::set_volume( const quint8 &val )
{

    if ( m_volume != val ) {
        writeProperty( QLatin1String("volume"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}


void WioGroveI2cFmReceiver::callMute(const bool &mute)
{
    callAction( QLatin1String("mute"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },mute);

}

QString WioGroveI2cFmReceiver::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveI2cFmReceiver%1")).arg(pin);
}

