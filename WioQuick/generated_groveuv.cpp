/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveuv.h"
#include <QJsonObject>

WioGroveUV::WioGroveUV( QObject *parent) : WioGroveModule(parent)
{
     m_uv_index = 0;

}

void WioGroveUV::update()
{

    readProperty( QLatin1String("uv_index"), [&](const QJsonObject &object){
       m_uv_index = object.toVariantMap()[QLatin1String("uv_index")].value<float>();

       Q_EMIT uvIndexChanged();
    });
}

float WioGroveUV::uv_index() const
{
    return m_uv_index;
}




QString WioGroveUV::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveUV%1")).arg(pin);
}

