/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovespeaker.h"
#include <QJsonObject>

WioGroveSpeaker::WioGroveSpeaker( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveSpeaker::update()
{

}



void WioGroveSpeaker::callSoundMs(const int &freq,const int &duration_ms)
{
    callAction( QLatin1String("sound_ms"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },freq,duration_ms);

}
void WioGroveSpeaker::callSoundStart(const int &freq)
{
    callAction( QLatin1String("sound_start"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },freq);

}
void WioGroveSpeaker::callSoundStop()
{
    callAction( QLatin1String("sound_stop"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}

QString WioGroveSpeaker::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveSpeaker%1")).arg(pin);
}

