/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEOLED9696_H
#define WIOGROVEOLED9696_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveOLED9696 : public WioGroveModule
{
    Q_OBJECT


public:
    WioGroveOLED9696( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callBase64String(const quint8 &row,const quint8 &col,const QString &b64_str);
    void callBrightness(const quint8 &brightness);
    void callClear();
    void callFloat(const quint8 &row,const quint8 &col,const float &f,const quint8 &decimal);
    void callInteger(const quint8 &row,const quint8 &col,const qint32 &i);
    void callInverseDisplay(const quint8 &inverse_or_not);
    void callScrollLeft(const quint8 &start_row,const quint8 &end_row,const quint8 &speed);
    void callScrollRight(const quint8 &start_row,const quint8 &end_row,const quint8 &speed);
    void callStopScroll();
    void callString(const quint8 &row,const quint8 &col,const QString &str);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVEOLED9696_H

