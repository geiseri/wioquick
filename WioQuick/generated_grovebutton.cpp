/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovebutton.h"
#include <QJsonObject>

WioGroveButton::WioGroveButton( QObject *parent) : WioGroveModule(parent)
{
     m_pressed = 0;

}

void WioGroveButton::update()
{

    readProperty( QLatin1String("pressed"), [&](const QJsonObject &object){
       m_pressed = object.toVariantMap()[QLatin1String("pressed")].value<quint8>();

       Q_EMIT pressedChanged();
    });
}

quint8 WioGroveButton::pressed() const
{
    return m_pressed;
}




QString WioGroveButton::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveButton%1")).arg(pin);
}

bool WioGroveButton::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("button_pressed") ) {
        Q_EMIT eventButtonPressed(data.value<qint16>());
        return true;
    }
    return false;
}

bool WioGroveButton::hasEvents( ) const
{
    return true;
}
