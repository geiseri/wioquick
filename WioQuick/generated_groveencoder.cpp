/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveencoder.h"
#include <QJsonObject>

WioGroveEncoder::WioGroveEncoder( QObject *parent) : WioGroveModule(parent)
{
     m_position = 0;

}

void WioGroveEncoder::update()
{

    readProperty( QLatin1String("position"), [&](const QJsonObject &object){
       m_position = object.toVariantMap()[QLatin1String("position")].value<qint32>();

       Q_EMIT positionChanged();
    });
}

qint32 WioGroveEncoder::position() const
{
    return m_position;
}



void WioGroveEncoder::callEnableAcceleration(const quint8 &enable)
{
    callAction( QLatin1String("enable_acceleration"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },enable);

}
void WioGroveEncoder::callResetPosition(const qint32 &position)
{
    callAction( QLatin1String("reset_position"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },position);

}

QString WioGroveEncoder::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveEncoder%1")).arg(pin);
}

bool WioGroveEncoder::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("encoder_position") ) {
        Q_EMIT eventEncoderPosition(data.value<qint32>());
        return true;
    }
    return false;
}

bool WioGroveEncoder::hasEvents( ) const
{
    return true;
}
