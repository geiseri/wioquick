/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveultraranger.h"
#include <QJsonObject>

WioGroveUltraRanger::WioGroveUltraRanger( QObject *parent) : WioGroveModule(parent)
{
     m_range_in_cm = 0;
     m_range_in_inch = 0;

}

void WioGroveUltraRanger::update()
{

    readProperty( QLatin1String("range_in_cm"), [&](const QJsonObject &object){
       m_range_in_cm = object.toVariantMap()[QLatin1String("range_cm")].value<float>();

       Q_EMIT rangeInCmChanged();
    });
    readProperty( QLatin1String("range_in_inch"), [&](const QJsonObject &object){
       m_range_in_inch = object.toVariantMap()[QLatin1String("range_inch")].value<float>();

       Q_EMIT rangeInInchChanged();
    });
}

float WioGroveUltraRanger::range_in_cm() const
{
    return m_range_in_cm;
}

float WioGroveUltraRanger::range_in_inch() const
{
    return m_range_in_inch;
}




QString WioGroveUltraRanger::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveUltraRanger%1")).arg(pin);
}

