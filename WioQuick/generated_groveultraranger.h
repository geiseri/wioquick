/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEULTRARANGER_H
#define WIOGROVEULTRARANGER_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveUltraRanger : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float rangeInCm READ range_in_cm NOTIFY rangeInCmChanged)
    Q_PROPERTY(float rangeInInch READ range_in_inch NOTIFY rangeInInchChanged)

public:
    WioGroveUltraRanger( QObject *parent = 0);

    float range_in_cm() const;
    float range_in_inch() const;

Q_SIGNALS:
    void rangeInCmChanged();
    void rangeInInchChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_range_in_cm;
    float m_range_in_inch;

};

#endif // WIOGROVEULTRARANGER_H

