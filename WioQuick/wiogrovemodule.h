/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEMODULE_H
#define WIOGROVEMODULE_H

#include <QObject>
#include <QQmlParserStatus>
#include <QVariant>
#include <QAbstractSocket>
#include <functional>

class QNetworkReply;
class QWebSocket;
class WioGroveModule : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged)
    Q_PROPERTY(QString pin READ pin WRITE setPin NOTIFY pinChanged)
    Q_PROPERTY(QString name READ sensorName NOTIFY pinChanged)

public:
    explicit WioGroveModule(QObject *parent = 0);
    virtual void classBegin();
    virtual void componentComplete();

    QString sensorName() const;
    QString lastError() const;

    QString pin() const;
    void setPin(const QString &pin);

public slots:
    virtual void update();

signals:
    void pinChanged();
    void lastErrorChanged();

protected:

    void readProperty( const QString &property,
                       std::function<void(QJsonObject)> functor,
                       const QVariant &arg1 = QVariant(),
                       const QVariant &arg2 = QVariant(),
                       const QVariant &arg3 = QVariant(),
                       const QVariant &arg4 = QVariant());

    void writeProperty(const QString &property,
                       std::function<void(QJsonObject)> functor,
                       const QVariant &arg1 = QVariant(),
                       const QVariant &arg2 = QVariant(),
                       const QVariant &arg3 = QVariant(),
                       const QVariant &arg4 = QVariant());

    void callAction( const QString &action,
                     std::function<void(QJsonObject)> functor,
                     const QVariant &arg1 = QVariant(),
                     const QVariant &arg2 = QVariant(),
                     const QVariant &arg3 = QVariant(),
                     const QVariant &arg4 = QVariant());

    bool checkError( const QJsonObject &obj );
    virtual QString buildSensorName( const QString &pin ) const;
    virtual bool emitEvent(const QString &event, const QVariant &data);
    virtual bool hasEvents() const;

private slots:
    void onOpenWebSocket();
    void onWebSocketError(const QAbstractSocket::SocketError &error);
    void onWebSocketMessage( const QString &msg);

private:
    void setLastError( const QString &err );
    void connectUpEventHandler();

    QWebSocket *m_webSocket;
    QString m_lastError;
    QString m_pin;
};

#endif // WIOGROVEMODULE_H
