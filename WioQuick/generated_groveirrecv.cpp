/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveirrecv.h"
#include <QJsonObject>

WioGroveIRRecv::WioGroveIRRecv( QObject *parent) : WioGroveModule(parent)
{
     
     

}

void WioGroveIRRecv::update()
{

    readProperty( QLatin1String("last_data_recved"), [&](const QJsonObject &object){
       m_last_data_recved[QLatin1String("len")] = object.toVariantMap()[QLatin1String("len")].value<quint16>();
       m_last_data_recved[QLatin1String("data")] = object.toVariantMap()[QLatin1String("data")].value<QString>();

       Q_EMIT lastDataRecvedChanged();
    });
    readProperty( QLatin1String("protocol_parameters"), [&](const QJsonObject &object){
       m_protocol_parameters[QLatin1String("start_h")] = object.toVariantMap()[QLatin1String("start_h")].value<quint8>();
       m_protocol_parameters[QLatin1String("start_l")] = object.toVariantMap()[QLatin1String("start_l")].value<quint8>();
       m_protocol_parameters[QLatin1String("n_short")] = object.toVariantMap()[QLatin1String("n_short")].value<quint8>();
       m_protocol_parameters[QLatin1String("n_long")] = object.toVariantMap()[QLatin1String("n_long")].value<quint8>();

       Q_EMIT protocolParametersChanged();
    });
}

QVariantMap WioGroveIRRecv::last_data_recved() const
{
    return m_last_data_recved;
}

QVariantMap WioGroveIRRecv::protocol_parameters() const
{
    return m_protocol_parameters;
}




QString WioGroveIRRecv::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveIRRecv%1")).arg(pin);
}

bool WioGroveIRRecv::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("ir_recv_data_hex") ) {
        Q_EMIT eventIrRecvDataHex(data.value<QString>());
        return true;
    }
    if ( event == QLatin1String("ir_recv_data_len") ) {
        Q_EMIT eventIrRecvDataLen(data.value<quint8>());
        return true;
    }
    return false;
}

bool WioGroveIRRecv::hasEvents( ) const
{
    return true;
}
