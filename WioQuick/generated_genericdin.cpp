/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_genericdin.h"
#include <QJsonObject>

WioGenericDIn::WioGenericDIn( QObject *parent) : WioGroveModule(parent)
{
     m_edge_fall_since_last_read = 0;
     m_edge_rise_since_last_read = 0;
     m_input = 0;

}

void WioGenericDIn::update()
{

    readProperty( QLatin1String("edge_fall_since_last_read"), [&](const QJsonObject &object){
       m_edge_fall_since_last_read = object.toVariantMap()[QLatin1String("falls")].value<quint32>();

       Q_EMIT edgeFallSinceLastReadChanged();
    });
    readProperty( QLatin1String("edge_rise_since_last_read"), [&](const QJsonObject &object){
       m_edge_rise_since_last_read = object.toVariantMap()[QLatin1String("rises")].value<quint32>();

       Q_EMIT edgeRiseSinceLastReadChanged();
    });
    readProperty( QLatin1String("input"), [&](const QJsonObject &object){
       m_input = object.toVariantMap()[QLatin1String("input")].value<quint8>();

       Q_EMIT inputChanged();
    });
}

quint32 WioGenericDIn::edge_fall_since_last_read() const
{
    return m_edge_fall_since_last_read;
}

quint32 WioGenericDIn::edge_rise_since_last_read() const
{
    return m_edge_rise_since_last_read;
}

quint8 WioGenericDIn::input() const
{
    return m_input;
}




QString WioGenericDIn::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GenericDIn%1")).arg(pin);
}

bool WioGenericDIn::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("input_changed") ) {
        Q_EMIT eventInputChanged(data.value<qint16>());
        return true;
    }
    if ( event == QLatin1String("input_fall") ) {
        Q_EMIT eventInputFall(data.value<qint16>());
        return true;
    }
    if ( event == QLatin1String("input_rise") ) {
        Q_EMIT eventInputRise(data.value<qint16>());
        return true;
    }
    return false;
}

bool WioGenericDIn::hasEvents( ) const
{
    return true;
}
