/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEELECMAGNET_H
#define WIOGROVEELECMAGNET_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveElecMagnet : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(int onoffStatus READ onoff_status NOTIFY onoffStatusChanged)

public:
    WioGroveElecMagnet( QObject *parent = 0);

    int onoff_status() const;

Q_SIGNALS:
    void onoffStatusChanged();


public Q_SLOTS:
    virtual void update();
    void callOnoff(const int &onoff);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    int m_onoff_status;

};

#endif // WIOGROVEELECMAGNET_H

