/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEI2CFMRECEIVER_H
#define WIOGROVEI2CFMRECEIVER_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveI2cFmReceiver : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(bool muteStatus READ mute_status NOTIFY muteStatusChanged)
    Q_PROPERTY(quint8 signalLevel READ signal_level NOTIFY signalLevelChanged)
    Q_PROPERTY(quint16 frequency READ frequency WRITE set_frequency NOTIFY frequencyChanged)
    Q_PROPERTY(quint8 volume READ volume WRITE set_volume NOTIFY volumeChanged)

public:
    WioGroveI2cFmReceiver( QObject *parent = 0);

    bool mute_status() const;
    quint8 signal_level() const;
    quint16 frequency() const;
    void set_frequency( const quint16 &val );
    quint8 volume() const;
    void set_volume( const quint8 &val );

Q_SIGNALS:
    void muteStatusChanged();
    void signalLevelChanged();
    void frequencyChanged();
    void volumeChanged();


public Q_SLOTS:
    virtual void update();
    void callMute(const bool &mute);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    bool m_mute_status;
    quint8 m_signal_level;
    quint16 m_frequency;
    quint8 m_volume;

};

#endif // WIOGROVEI2CFMRECEIVER_H

