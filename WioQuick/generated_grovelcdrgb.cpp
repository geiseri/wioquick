/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovelcdrgb.h"
#include <QJsonObject>

WioGroveLCDRGB::WioGroveLCDRGB( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveLCDRGB::update()
{

}



void WioGroveLCDRGB::callBacklightColor(const quint8 &color_index)
{
    callAction( QLatin1String("backlight_color"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },color_index);

}
void WioGroveLCDRGB::callBacklightColorRgb(const quint8 &r,const quint8 &g,const quint8 &b)
{
    callAction( QLatin1String("backlight_color_rgb"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },r,g,b);

}
void WioGroveLCDRGB::callBase64String(const quint8 &row,const quint8 &col,const QString &b64_str)
{
    callAction( QLatin1String("base64_string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,b64_str);

}
void WioGroveLCDRGB::callClear()
{
    callAction( QLatin1String("clear"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveLCDRGB::callDisplayOff()
{
    callAction( QLatin1String("display_off"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveLCDRGB::callDisplayOn()
{
    callAction( QLatin1String("display_on"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveLCDRGB::callFloat(const quint8 &row,const quint8 &col,const float &f,const quint8 &decimal)
{
    callAction( QLatin1String("float"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,f,decimal);

}
void WioGroveLCDRGB::callInteger(const quint8 &row,const quint8 &col,const qint32 &i)
{
    callAction( QLatin1String("integer"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,i);

}
void WioGroveLCDRGB::callScrollLeft(const quint8 &speed)
{
    callAction( QLatin1String("scroll_left"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },speed);

}
void WioGroveLCDRGB::callScrollRight(const quint8 &speed)
{
    callAction( QLatin1String("scroll_right"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },speed);

}
void WioGroveLCDRGB::callStopScroll()
{
    callAction( QLatin1String("stop_scroll"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveLCDRGB::callString(const quint8 &row,const quint8 &col,const QString &str)
{
    callAction( QLatin1String("string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,str);

}

QString WioGroveLCDRGB::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveLCDRGB%1")).arg(pin);
}

