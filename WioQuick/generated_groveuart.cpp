/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveuart.h"
#include <QJsonObject>

WioGroveUART::WioGroveUART( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveUART::update()
{

}



void WioGroveUART::callBase64String(const QString &b64_str)
{
    callAction( QLatin1String("base64_string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },b64_str);

}
void WioGroveUART::callBaudrate(const quint8 &index)
{
    callAction( QLatin1String("baudrate"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },index);

}
void WioGroveUART::callString(const QString &str)
{
    callAction( QLatin1String("string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },str);

}

QString WioGroveUART::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveUART%1")).arg(pin);
}

bool WioGroveUART::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("uart_rx") ) {
        Q_EMIT eventUartRx(data.value<QString>());
        return true;
    }
    return false;
}

bool WioGroveUART::hasEvents( ) const
{
    return true;
}
