/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveledws2812.h"
#include <QJsonObject>

WioGroveLedWs2812::WioGroveLedWs2812( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveLedWs2812::update()
{

}



void WioGroveLedWs2812::callClear(const quint8 &total_led_cnt,const QString &rgb_hex_string)
{
    callAction( QLatin1String("clear"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },total_led_cnt,rgb_hex_string);

}
void WioGroveLedWs2812::callSegment(const quint8 &start,const QString &rgb_hex_string)
{
    callAction( QLatin1String("segment"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },start,rgb_hex_string);

}
void WioGroveLedWs2812::callStartRainbowFlow(const quint8 &length,const quint8 &brightness,const quint8 &speed)
{
    callAction( QLatin1String("start_rainbow_flow"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },length,brightness,speed);

}
void WioGroveLedWs2812::callStopRainbowFlow()
{
    callAction( QLatin1String("stop_rainbow_flow"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}

QString WioGroveLedWs2812::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveLedWs2812%1")).arg(pin);
}

