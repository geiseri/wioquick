/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEEXAMPLE_H
#define WIOGROVEEXAMPLE_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveExample : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap acc READ acc NOTIFY accChanged)
    Q_PROPERTY(QVariantMap compass READ compass NOTIFY compassChanged)
    Q_PROPERTY(float humidity READ humidity NOTIFY humidityChanged)
    Q_PROPERTY(int temp READ temp NOTIFY tempChanged)
    Q_PROPERTY(quint8 uint8Value READ uint8_value NOTIFY uint8ValueChanged)
    Q_PROPERTY(QVariantMap withArg READ with_arg NOTIFY withArgChanged)

public:
    WioGroveExample( QObject *parent = 0);

    QVariantMap acc() const;
    QVariantMap compass() const;
    float humidity() const;
    int temp() const;
    quint8 uint8_value() const;
    QVariantMap with_arg() const;

Q_SIGNALS:
    void accChanged();
    void compassChanged();
    void humidityChanged();
    void tempChanged();
    void uint8ValueChanged();
    void withArgChanged();

    void eventEvent2(const qint16 &value);
    void eventFire(const qint16 &value);

public Q_SLOTS:
    virtual void update();
    void callAccMode(const quint8 &mode);
    void callFloatValue(const float &f);
    void callMultiValue(const int &a,const float &b,const quint32 &c);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
    virtual bool emitEvent( const QString &event, const QVariant &data );
    virtual bool hasEvents() const;
private:
    QVariantMap m_acc;
    QVariantMap m_compass;
    float m_humidity;
    int m_temp;
    quint8 m_uint8_value;
    QVariantMap m_with_arg;

};

#endif // WIOGROVEEXAMPLE_H

