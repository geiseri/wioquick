/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveservo.h"
#include <QJsonObject>

WioGroveServo::WioGroveServo( QObject *parent) : WioGroveModule(parent)
{
     m_angle = 0;

}

void WioGroveServo::update()
{

    readProperty( QLatin1String("angle"), [&](const QJsonObject &object){
       m_angle = object.toVariantMap()[QLatin1String("degree")].value<int>();

       Q_EMIT angleChanged();
    });
}


int WioGroveServo::angle() const
{
    return m_angle;
}

void WioGroveServo::set_angle( const int &val )
{

    if ( m_angle != val ) {
        writeProperty( QLatin1String("angle"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}


void WioGroveServo::callAngleMotionInSeconds(const int &degree,const int &seconds)
{
    callAction( QLatin1String("angle_motion_in_seconds"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },degree,seconds);

}

QString WioGroveServo::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveServo%1")).arg(pin);
}

