/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEMULTICHANNELGAS_H
#define WIOGROVEMULTICHANNELGAS_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveMultiChannelGas : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float c2H5Oh READ C2H5OH NOTIFY c2H5OhChanged)
    Q_PROPERTY(float c3H8 READ C3H8 NOTIFY c3H8Changed)
    Q_PROPERTY(float c4H10 READ C4H10 NOTIFY c4H10Changed)
    Q_PROPERTY(float ch4 READ CH4 NOTIFY ch4Changed)
    Q_PROPERTY(float co READ CO NOTIFY coChanged)
    Q_PROPERTY(float h2 READ H2 NOTIFY h2Changed)
    Q_PROPERTY(float nh3 READ NH3 NOTIFY nh3Changed)
    Q_PROPERTY(float no2 READ NO2 NOTIFY no2Changed)

public:
    WioGroveMultiChannelGas( QObject *parent = 0);

    float C2H5OH() const;
    float C3H8() const;
    float C4H10() const;
    float CH4() const;
    float CO() const;
    float H2() const;
    float NH3() const;
    float NO2() const;

Q_SIGNALS:
    void c2H5OhChanged();
    void c3H8Changed();
    void c4H10Changed();
    void ch4Changed();
    void coChanged();
    void h2Changed();
    void nh3Changed();
    void no2Changed();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_C2H5OH;
    float m_C3H8;
    float m_C4H10;
    float m_CH4;
    float m_CO;
    float m_H2;
    float m_NH3;
    float m_NO2;

};

#endif // WIOGROVEMULTICHANNELGAS_H

