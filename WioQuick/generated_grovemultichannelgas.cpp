/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovemultichannelgas.h"
#include <QJsonObject>

WioGroveMultiChannelGas::WioGroveMultiChannelGas( QObject *parent) : WioGroveModule(parent)
{
     m_C2H5OH = 0;
     m_C3H8 = 0;
     m_C4H10 = 0;
     m_CH4 = 0;
     m_CO = 0;
     m_H2 = 0;
     m_NH3 = 0;
     m_NO2 = 0;

}

void WioGroveMultiChannelGas::update()
{

    readProperty( QLatin1String("C2H5OH"), [&](const QJsonObject &object){
       m_C2H5OH = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT c2H5OhChanged();
    });
    readProperty( QLatin1String("C3H8"), [&](const QJsonObject &object){
       m_C3H8 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT c3H8Changed();
    });
    readProperty( QLatin1String("C4H10"), [&](const QJsonObject &object){
       m_C4H10 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT c4H10Changed();
    });
    readProperty( QLatin1String("CH4"), [&](const QJsonObject &object){
       m_CH4 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT ch4Changed();
    });
    readProperty( QLatin1String("CO"), [&](const QJsonObject &object){
       m_CO = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT coChanged();
    });
    readProperty( QLatin1String("H2"), [&](const QJsonObject &object){
       m_H2 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT h2Changed();
    });
    readProperty( QLatin1String("NH3"), [&](const QJsonObject &object){
       m_NH3 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT nh3Changed();
    });
    readProperty( QLatin1String("NO2"), [&](const QJsonObject &object){
       m_NO2 = object.toVariantMap()[QLatin1String("concentration_ppm")].value<float>();

       Q_EMIT no2Changed();
    });
}

float WioGroveMultiChannelGas::C2H5OH() const
{
    return m_C2H5OH;
}

float WioGroveMultiChannelGas::C3H8() const
{
    return m_C3H8;
}

float WioGroveMultiChannelGas::C4H10() const
{
    return m_C4H10;
}

float WioGroveMultiChannelGas::CH4() const
{
    return m_CH4;
}

float WioGroveMultiChannelGas::CO() const
{
    return m_CO;
}

float WioGroveMultiChannelGas::H2() const
{
    return m_H2;
}

float WioGroveMultiChannelGas::NH3() const
{
    return m_NH3;
}

float WioGroveMultiChannelGas::NO2() const
{
    return m_NO2;
}




QString WioGroveMultiChannelGas::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveMultiChannelGas%1")).arg(pin);
}

