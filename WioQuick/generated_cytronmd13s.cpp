/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_cytronmd13s.h"
#include <QJsonObject>

WioCytronMD13S::WioCytronMD13S( QObject *parent) : WioGroveModule(parent)
{
     m_direction = 0;
     m_speed = 0;
     

}

void WioCytronMD13S::update()
{

    readProperty( QLatin1String("direction"), [&](const QJsonObject &object){
       m_direction = object.toVariantMap()[QLatin1String("direction")].value<int>();

       Q_EMIT directionChanged();
    });
    readProperty( QLatin1String("speed"), [&](const QJsonObject &object){
       m_speed = object.toVariantMap()[QLatin1String("speed")].value<float>();

       Q_EMIT speedChanged();
    });
    readProperty( QLatin1String("speed_dir"), [&](const QJsonObject &object){
       m_speed_dir[QLatin1String("speed")] = object.toVariantMap()[QLatin1String("speed")].value<float>();
       m_speed_dir[QLatin1String("direction")] = object.toVariantMap()[QLatin1String("direction")].value<int>();

       Q_EMIT speedDirChanged();
    });
}


int WioCytronMD13S::direction() const
{
    return m_direction;
}

void WioCytronMD13S::set_direction( const int &val )
{

    if ( m_direction != val ) {
        writeProperty( QLatin1String("direction"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}

float WioCytronMD13S::speed() const
{
    return m_speed;
}

void WioCytronMD13S::set_speed( const float &val )
{

    if ( m_speed != val ) {
        writeProperty( QLatin1String("speed"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val);
    }

}

QVariantMap WioCytronMD13S::speed_dir() const
{
    return m_speed_dir;
}

void WioCytronMD13S::set_speed_dir( const QVariantMap &val )
{

    if ( m_speed_dir != val ) {
        writeProperty( QLatin1String("speed_dir"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        },val[QLatin1String("speed")],val[QLatin1String("direction")]);
    }

}



QString WioCytronMD13S::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("CytronMD13S%1")).arg(pin);
}

