/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEACCMMA7660_H
#define WIOGROVEACCMMA7660_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveAccMMA7660 : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap acceleration READ acceleration NOTIFY accelerationChanged)
    Q_PROPERTY(quint8 shaked READ shaked NOTIFY shakedChanged)

public:
    WioGroveAccMMA7660( QObject *parent = 0);

    QVariantMap acceleration() const;
    quint8 shaked() const;

Q_SIGNALS:
    void accelerationChanged();
    void shakedChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    QVariantMap m_acceleration;
    quint8 m_shaked;

};

#endif // WIOGROVEACCMMA7660_H

