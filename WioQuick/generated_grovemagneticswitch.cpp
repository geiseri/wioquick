/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovemagneticswitch.h"
#include <QJsonObject>

WioGroveMagneticSwitch::WioGroveMagneticSwitch( QObject *parent) : WioGroveModule(parent)
{
     m_approach = 0;

}

void WioGroveMagneticSwitch::update()
{

    readProperty( QLatin1String("approach"), [&](const QJsonObject &object){
       m_approach = object.toVariantMap()[QLatin1String("mag_approach")].value<quint8>();

       Q_EMIT approachChanged();
    });
}

quint8 WioGroveMagneticSwitch::approach() const
{
    return m_approach;
}




QString WioGroveMagneticSwitch::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveMagneticSwitch%1")).arg(pin);
}

bool WioGroveMagneticSwitch::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("mag_approached") ) {
        Q_EMIT eventMagApproached(data.value<quint8>());
        return true;
    }
    return false;
}

bool WioGroveMagneticSwitch::hasEvents( ) const
{
    return true;
}
