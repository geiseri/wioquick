/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovecompass.h"
#include <QJsonObject>

WioGroveCompass::WioGroveCompass( QObject *parent) : WioGroveModule(parent)
{
     m_compass_heading = 0;

}

void WioGroveCompass::update()
{

    readProperty( QLatin1String("compass_heading"), [&](const QJsonObject &object){
       m_compass_heading = object.toVariantMap()[QLatin1String("heading_deg")].value<float>();

       Q_EMIT compassHeadingChanged();
    });
}

float WioGroveCompass::compass_heading() const
{
    return m_compass_heading;
}




QString WioGroveCompass::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveCompass%1")).arg(pin);
}

