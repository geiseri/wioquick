/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEIREMIT_H
#define WIOGROVEIREMIT_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveIREmit : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap protocalParameters READ protocal_parameters NOTIFY protocalParametersChanged)

public:
    WioGroveIREmit( QObject *parent = 0);

    QVariantMap protocal_parameters() const;

Q_SIGNALS:
    void protocalParametersChanged();


public Q_SLOTS:
    virtual void update();
    void callDataHex(const QString &data_hex);
    void callDataHexInFreq(const quint16 &freq_khz,const QString &data_hex);
    void callProtocolParameters(const quint8 &start_h,const quint8 &start_l,const quint8 &n_short,const quint8 &n_long);

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    QVariantMap m_protocal_parameters;

};

#endif // WIOGROVEIREMIT_H

