/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grove4digit.h"
#include <QJsonObject>

WioGrove4Digit::WioGrove4Digit( QObject *parent) : WioGroveModule(parent)
{

}

void WioGrove4Digit::update()
{

}



void WioGrove4Digit::callBrightness(const quint8 &brightness)
{
    callAction( QLatin1String("brightness"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },brightness);

}
void WioGrove4Digit::callClear()
{
    callAction( QLatin1String("clear"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGrove4Digit::callDisplayDigits(const quint8 &start_pos,const QString &chars)
{
    callAction( QLatin1String("display_digits"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },start_pos,chars);

}
void WioGrove4Digit::callDisplayOneDigit(const quint8 &position,const QString &chr)
{
    callAction( QLatin1String("display_one_digit"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },position,chr);

}
void WioGrove4Digit::callDisplayPoint(const quint8 &display)
{
    callAction( QLatin1String("display_point"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },display);

}

QString WioGrove4Digit::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("Grove4Digit%1")).arg(pin);
}

