/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovemp3v2.h"
#include <QJsonObject>

WioGroveMP3V2::WioGroveMP3V2( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveMP3V2::update()
{

}



void WioGroveMP3V2::callIndexToPlay(const quint8 &index)
{
    callAction( QLatin1String("index_to_play"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },index);

}
void WioGroveMP3V2::callLoopAll()
{
    callAction( QLatin1String("loop_all"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveMP3V2::callNext()
{
    callAction( QLatin1String("next"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveMP3V2::callPause()
{
    callAction( QLatin1String("pause"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveMP3V2::callPrev()
{
    callAction( QLatin1String("prev"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveMP3V2::callResume()
{
    callAction( QLatin1String("resume"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveMP3V2::callVolume(const quint8 &volume)
{
    callAction( QLatin1String("volume"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },volume);

}

QString WioGroveMP3V2::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveMP3V2%1")).arg(pin);
}

