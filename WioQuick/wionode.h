/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIONODE_H
#define WIONODE_H

#include <QObject>
#include <QQmlParserStatus>
#include <QQmlListProperty>
#include "wiogrovemodule.h"

class QNetworkRequest;

class WioNode : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_CLASSINFO("DefaultProperty", "modules")

    Q_PROPERTY(QQmlListProperty<WioGroveModule> modules READ modules)
    Q_PROPERTY(QString dataExchangeServer READ dataExchangeServer WRITE setDataExchangeServer NOTIFY dataExchangeServerChanged)
    Q_PROPERTY(QString name READ nodeName WRITE setNodeName NOTIFY nodeNameChanged)
    Q_PROPERTY(QString userToken READ userToken WRITE setUserToken NOTIFY userTokenChanged)
    Q_PROPERTY(QString key READ nodeKey NOTIFY nodeKeyChanged)
    Q_PROPERTY(QString model READ nodeBoardName NOTIFY nodeBoardNameChanged)
    Q_PROPERTY(QString serialNumber READ nodeSerialNumber NOTIFY nodeSerialNumberChanged)
    Q_PROPERTY(bool online READ nodeOnline NOTIFY nodeOnlineChanged)
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY isValidChanged)

public:
    WioNode(QObject *parent = 0);

    QQmlListProperty<WioGroveModule> modules();

    QString nodeName() const;
    void setNodeName( const QString &name );

    QString userToken() const;
    void setUserToken(const QString &token);

    QString nodeKey() const;
    void setNodeKey(const QString &key);

    QString dataExchangeServer() const;
    void setDataExchangeServer(const QString &dataExchangeServer);


    QString nodeBoardName() const;
    void setNodeBoardName(const QString &nodeBoardName);

    QString nodeSerialNumber() const;
    void setNodeSerialNumber(const QString &nodeSerialNumber);

    bool nodeOnline() const;
    void setNodeOnline(bool nodeOnline);

    bool isValid() const;

    QNetworkRequest baseNetworkRequest( const QString &path) const;
    QUrl serverUrl( const QString &protocol, const QString &path ) const;

    QString lastError() const;

public slots:
    void sleep( const quint32 &seconds);
    void update();

signals:
    void nodeNameChanged();
    void nodeKeyChanged();
    void nodeBoardNameChanged();
    void nodeSerialNumberChanged();
    void nodeOnlineChanged();
    void userTokenChanged();
    void dataExchangeServerChanged();
    void isValidChanged();
    void sleepSuccessful();
    void modulesChanged();
    void lastErrorChanged();

private:
    QByteArray getNodeToken() const;
    QByteArray getUserToken() const;
    QUrl getUrlForServer(const QString &path) const;
    void setIsValid( bool valid );
    void setLastError( const QString &err );
    void resolveNodeKey();
    void updateBoardInfo();

private:
    QList<WioGroveModule*> m_modules;

    QString m_nodeName;
    QString m_nodeKey;
    QString m_nodeBoardName;
    QString m_nodeSerialNumber;
    bool m_nodeOnline;
    QString m_userToken;
    QString m_dataExchangeServer;
    QString m_lastError;
    bool m_isValid;

    // QQmlParserStatus interface
public:
    virtual void classBegin();
    virtual void componentComplete();

};

#endif // WIONODE_H
