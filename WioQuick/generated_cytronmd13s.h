/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOCYTRONMD13S_H
#define WIOCYTRONMD13S_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioCytronMD13S : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(int direction READ direction WRITE set_direction NOTIFY directionChanged)
    Q_PROPERTY(float speed READ speed WRITE set_speed NOTIFY speedChanged)
    Q_PROPERTY(QVariantMap speedDir READ speed_dir WRITE set_speed_dir NOTIFY speedDirChanged)

public:
    WioCytronMD13S( QObject *parent = 0);

    int direction() const;
    void set_direction( const int &val );
    float speed() const;
    void set_speed( const float &val );
    QVariantMap speed_dir() const;
    void set_speed_dir( const QVariantMap &val );

Q_SIGNALS:
    void directionChanged();
    void speedChanged();
    void speedDirChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    int m_direction;
    float m_speed;
    QVariantMap m_speed_dir;

};

#endif // WIOCYTRONMD13S_H

