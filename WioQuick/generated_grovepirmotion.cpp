/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovepirmotion.h"
#include <QJsonObject>

WioGrovePIRMotion::WioGrovePIRMotion( QObject *parent) : WioGroveModule(parent)
{
     m_approach = 0;

}

void WioGrovePIRMotion::update()
{

    readProperty( QLatin1String("approach"), [&](const QJsonObject &object){
       m_approach = object.toVariantMap()[QLatin1String("approach")].value<quint8>();

       Q_EMIT approachChanged();
    });
}

quint8 WioGrovePIRMotion::approach() const
{
    return m_approach;
}




QString WioGrovePIRMotion::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GrovePIRMotion%1")).arg(pin);
}

bool WioGrovePIRMotion::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("ir_moved") ) {
        Q_EMIT eventIrMoved(data.value<qint16>());
        return true;
    }
    return false;
}

bool WioGrovePIRMotion::hasEvents( ) const
{
    return true;
}
