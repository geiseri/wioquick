TEMPLATE = lib
TARGET = wio_plugin
QT += qml quick websockets
CONFIG += qt plugin

TARGET = $$qtLibraryTarget($$TARGET)
uri = Seeed.Wio

DISTFILES = qmldir

DESTDIR = $$replace(uri, \\., /)

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/$$DESTDIR/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}


# Input
OTHER_FILES += qmldir \
               drivers.json \
               generate_types.py


DISTFILES += \
    generate_types.py \
    drivers.json \
    qmldir


SOURCES += \
            wiogrovemodule.cpp \
            wionode.cpp

HEADERS += \
            wiogrovemodule.h \
            wionode.h

include (generated.pri)
