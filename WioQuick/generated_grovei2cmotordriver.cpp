/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovei2cmotordriver.h"
#include <QJsonObject>

WioGroveI2CMotorDriver::WioGroveI2CMotorDriver( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveI2CMotorDriver::update()
{

}



void WioGroveI2CMotorDriver::callDcmotor1Break()
{
    callAction( QLatin1String("dcmotor1_break"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotor1ChangeDirection()
{
    callAction( QLatin1String("dcmotor1_change_direction"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotor1Resume()
{
    callAction( QLatin1String("dcmotor1_resume"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotor2Break()
{
    callAction( QLatin1String("dcmotor2_break"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotor2ChangeDirection()
{
    callAction( QLatin1String("dcmotor2_change_direction"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotor2Resume()
{
    callAction( QLatin1String("dcmotor2_resume"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callDcmotorSpeed(const quint8 &speed_m1,const quint8 &speed_m2)
{
    callAction( QLatin1String("dcmotor_speed"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },speed_m1,speed_m2);

}
void WioGroveI2CMotorDriver::callDisableStepperMode()
{
    callAction( QLatin1String("disable_stepper_mode"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveI2CMotorDriver::callEnableStepperMode(const quint8 &direction,const quint8 &speed)
{
    callAction( QLatin1String("enable_stepper_mode"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },direction,speed);

}
void WioGroveI2CMotorDriver::callI2CAddress(const quint8 &addr_7bits)
{
    callAction( QLatin1String("i2c_address"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },addr_7bits);

}
void WioGroveI2CMotorDriver::callStepperSteps(const quint8 &steps)
{
    callAction( QLatin1String("stepper_steps"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },steps);

}

QString WioGroveI2CMotorDriver::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveI2CMotorDriver%1")).arg(pin);
}

