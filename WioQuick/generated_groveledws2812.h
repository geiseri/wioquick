/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVELEDWS2812_H
#define WIOGROVELEDWS2812_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveLedWs2812 : public WioGroveModule
{
    Q_OBJECT


public:
    WioGroveLedWs2812( QObject *parent = 0);


Q_SIGNALS:


public Q_SLOTS:
    virtual void update();
    void callClear(const quint8 &total_led_cnt,const QString &rgb_hex_string);
    void callSegment(const quint8 &start,const QString &rgb_hex_string);
    void callStartRainbowFlow(const quint8 &length,const quint8 &brightness,const quint8 &speed);
    void callStopRainbowFlow();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:

};

#endif // WIOGROVELEDWS2812_H

