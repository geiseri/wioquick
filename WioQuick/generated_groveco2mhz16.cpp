/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveco2mhz16.h"
#include <QJsonObject>

WioGroveCo2MhZ16::WioGroveCo2MhZ16( QObject *parent) : WioGroveModule(parent)
{
     m_concentration = 0;
     
     m_temperature = 0;

}

void WioGroveCo2MhZ16::update()
{

    readProperty( QLatin1String("concentration"), [&](const QJsonObject &object){
       m_concentration = object.toVariantMap()[QLatin1String("concentration")].value<float>();

       Q_EMIT concentrationChanged();
    });
    readProperty( QLatin1String("concentration_and_temperature"), [&](const QJsonObject &object){
       m_concentration_and_temperature[QLatin1String("concentration")] = object.toVariantMap()[QLatin1String("concentration")].value<float>();
       m_concentration_and_temperature[QLatin1String("temperature")] = object.toVariantMap()[QLatin1String("temperature")].value<float>();

       Q_EMIT concentrationAndTemperatureChanged();
    });
    readProperty( QLatin1String("temperature"), [&](const QJsonObject &object){
       m_temperature = object.toVariantMap()[QLatin1String("temperature")].value<float>();

       Q_EMIT temperatureChanged();
    });
}

float WioGroveCo2MhZ16::concentration() const
{
    return m_concentration;
}

QVariantMap WioGroveCo2MhZ16::concentration_and_temperature() const
{
    return m_concentration_and_temperature;
}

float WioGroveCo2MhZ16::temperature() const
{
    return m_temperature;
}




QString WioGroveCo2MhZ16::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveCo2MhZ16%1")).arg(pin);
}

