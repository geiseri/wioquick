/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEBME280_H
#define WIOGROVEBME280_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveBME280 : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float altitude READ altitude NOTIFY altitudeChanged)
    Q_PROPERTY(quint32 humidity READ humidity NOTIFY humidityChanged)
    Q_PROPERTY(quint32 pressure READ pressure NOTIFY pressureChanged)
    Q_PROPERTY(float temperature READ temperature NOTIFY temperatureChanged)

public:
    WioGroveBME280( QObject *parent = 0);

    float altitude() const;
    quint32 humidity() const;
    quint32 pressure() const;
    float temperature() const;

Q_SIGNALS:
    void altitudeChanged();
    void humidityChanged();
    void pressureChanged();
    void temperatureChanged();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_altitude;
    quint32 m_humidity;
    quint32 m_pressure;
    float m_temperature;

};

#endif // WIOGROVEBME280_H

