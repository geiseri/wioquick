/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovesound.h"
#include <QJsonObject>

WioGroveSound::WioGroveSound( QObject *parent) : WioGroveModule(parent)
{
     m_sound_level = 0;

}

void WioGroveSound::update()
{

    readProperty( QLatin1String("sound_level"), [&](const QJsonObject &object){
       m_sound_level = object.toVariantMap()[QLatin1String("sound_level")].value<int>();

       Q_EMIT soundLevelChanged();
    });
}

int WioGroveSound::sound_level() const
{
    return m_sound_level;
}




QString WioGroveSound::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveSound%1")).arg(pin);
}

