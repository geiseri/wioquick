/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveexample.h"
#include <QJsonObject>

WioGroveExample::WioGroveExample( QObject *parent) : WioGroveModule(parent)
{
     
     
     m_humidity = 0;
     m_temp = 0;
     m_uint8_value = 0;
     

}

void WioGroveExample::update()
{

    readProperty( QLatin1String("acc"), [&](const QJsonObject &object){
       m_acc[QLatin1String("ax")] = object.toVariantMap()[QLatin1String("ax")].value<float>();
       m_acc[QLatin1String("ay")] = object.toVariantMap()[QLatin1String("ay")].value<float>();
       m_acc[QLatin1String("az")] = object.toVariantMap()[QLatin1String("az")].value<float>();

       Q_EMIT accChanged();
    });
    readProperty( QLatin1String("compass"), [&](const QJsonObject &object){
       m_compass[QLatin1String("cx")] = object.toVariantMap()[QLatin1String("cx")].value<float>();
       m_compass[QLatin1String("cy")] = object.toVariantMap()[QLatin1String("cy")].value<float>();
       m_compass[QLatin1String("cz")] = object.toVariantMap()[QLatin1String("cz")].value<float>();
       m_compass[QLatin1String("degree")] = object.toVariantMap()[QLatin1String("degree")].value<int>();

       Q_EMIT compassChanged();
    });
    readProperty( QLatin1String("humidity"), [&](const QJsonObject &object){
       m_humidity = object.toVariantMap()[QLatin1String("humidity")].value<float>();

       Q_EMIT humidityChanged();
    });
    readProperty( QLatin1String("temp"), [&](const QJsonObject &object){
       m_temp = object.toVariantMap()[QLatin1String("temp")].value<int>();

       Q_EMIT tempChanged();
    });
    readProperty( QLatin1String("uint8_value"), [&](const QJsonObject &object){
       m_uint8_value = object.toVariantMap()[QLatin1String("value")].value<quint8>();

       Q_EMIT uint8ValueChanged();
    });
    readProperty( QLatin1String("with_arg"), [&](const QJsonObject &object){
       m_with_arg[QLatin1String("cx")] = object.toVariantMap()[QLatin1String("cx")].value<float>();
       m_with_arg[QLatin1String("cy")] = object.toVariantMap()[QLatin1String("cy")].value<float>();
       m_with_arg[QLatin1String("cz")] = object.toVariantMap()[QLatin1String("cz")].value<float>();
       m_with_arg[QLatin1String("degree")] = object.toVariantMap()[QLatin1String("degree")].value<int>();

       Q_EMIT withArgChanged();
    });
}

QVariantMap WioGroveExample::acc() const
{
    return m_acc;
}

QVariantMap WioGroveExample::compass() const
{
    return m_compass;
}

float WioGroveExample::humidity() const
{
    return m_humidity;
}

int WioGroveExample::temp() const
{
    return m_temp;
}

quint8 WioGroveExample::uint8_value() const
{
    return m_uint8_value;
}

QVariantMap WioGroveExample::with_arg() const
{
    return m_with_arg;
}



void WioGroveExample::callAccMode(const quint8 &mode)
{
    callAction( QLatin1String("acc_mode"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },mode);

}
void WioGroveExample::callFloatValue(const float &f)
{
    callAction( QLatin1String("float_value"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },f);

}
void WioGroveExample::callMultiValue(const int &a,const float &b,const quint32 &c)
{
    callAction( QLatin1String("multi_value"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },a,b,c);

}

QString WioGroveExample::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveExample%1")).arg(pin);
}

bool WioGroveExample::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("event2") ) {
        Q_EMIT eventEvent2(data.value<qint16>());
        return true;
    }
    if ( event == QLatin1String("fire") ) {
        Q_EMIT eventFire(data.value<qint16>());
        return true;
    }
    return false;
}

bool WioGroveExample::hasEvents( ) const
{
    return true;
}
