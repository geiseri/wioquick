/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveiremit.h"
#include <QJsonObject>

WioGroveIREmit::WioGroveIREmit( QObject *parent) : WioGroveModule(parent)
{
     

}

void WioGroveIREmit::update()
{

    readProperty( QLatin1String("protocal_parameters"), [&](const QJsonObject &object){
       m_protocal_parameters[QLatin1String("start_h")] = object.toVariantMap()[QLatin1String("start_h")].value<quint8>();
       m_protocal_parameters[QLatin1String("start_l")] = object.toVariantMap()[QLatin1String("start_l")].value<quint8>();
       m_protocal_parameters[QLatin1String("n_short")] = object.toVariantMap()[QLatin1String("n_short")].value<quint8>();
       m_protocal_parameters[QLatin1String("n_long")] = object.toVariantMap()[QLatin1String("n_long")].value<quint8>();

       Q_EMIT protocalParametersChanged();
    });
}

QVariantMap WioGroveIREmit::protocal_parameters() const
{
    return m_protocal_parameters;
}



void WioGroveIREmit::callDataHex(const QString &data_hex)
{
    callAction( QLatin1String("data_hex"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },data_hex);

}
void WioGroveIREmit::callDataHexInFreq(const quint16 &freq_khz,const QString &data_hex)
{
    callAction( QLatin1String("data_hex_in_freq"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },freq_khz,data_hex);

}
void WioGroveIREmit::callProtocolParameters(const quint8 &start_h,const quint8 &start_l,const quint8 &n_short,const quint8 &n_long)
{
    callAction( QLatin1String("protocol_parameters"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },start_h,start_l,n_short,n_long);

}

QString WioGroveIREmit::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveIREmit%1")).arg(pin);
}

