/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIOGROVEVOLDIVIDER_H
#define WIOGROVEVOLDIVIDER_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class WioGroveVolDivider : public WioGroveModule
{
    Q_OBJECT

    Q_PROPERTY(float voltageX10 READ voltage_x10 NOTIFY voltageX10Changed)
    Q_PROPERTY(float voltageX3 READ voltage_x3 NOTIFY voltageX3Changed)

public:
    WioGroveVolDivider( QObject *parent = 0);

    float voltage_x10() const;
    float voltage_x3() const;

Q_SIGNALS:
    void voltageX10Changed();
    void voltageX3Changed();


public Q_SLOTS:
    virtual void update();

protected:
    virtual QString buildSensorName( const QString &pin ) const;
private:
    float m_voltage_x10;
    float m_voltage_x3;

};

#endif // WIOGROVEVOLDIVIDER_H

