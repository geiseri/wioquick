/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovegesture.h"
#include <QJsonObject>

WioGroveGesture::WioGroveGesture( QObject *parent) : WioGroveModule(parent)
{
     m_motion = 0;

}

void WioGroveGesture::update()
{

    readProperty( QLatin1String("motion"), [&](const QJsonObject &object){
       m_motion = object.toVariantMap()[QLatin1String("motion")].value<quint8>();

       Q_EMIT motionChanged();
    });
}

quint8 WioGroveGesture::motion() const
{
    return m_motion;
}




QString WioGroveGesture::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveGesture%1")).arg(pin);
}

bool WioGroveGesture::emitEvent( const QString &event, const QVariant &data )
{
    if ( event == QLatin1String("gesture") ) {
        Q_EMIT eventGesture(data.value<quint8>());
        return true;
    }
    return false;
}

bool WioGroveGesture::hasEvents( ) const
{
    return true;
}
