/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_groveoled12864.h"
#include <QJsonObject>

WioGroveOLED12864::WioGroveOLED12864( QObject *parent) : WioGroveModule(parent)
{

}

void WioGroveOLED12864::update()
{

}



void WioGroveOLED12864::callBase64String(const quint8 &row,const quint8 &col,const QString &b64_str)
{
    callAction( QLatin1String("base64_string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,b64_str);

}
void WioGroveOLED12864::callBrightness(const quint8 &brightness)
{
    callAction( QLatin1String("brightness"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },brightness);

}
void WioGroveOLED12864::callClear()
{
    callAction( QLatin1String("clear"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveOLED12864::callFloat(const quint8 &row,const quint8 &col,const float &f,const quint8 &decimal)
{
    callAction( QLatin1String("float"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,f,decimal);

}
void WioGroveOLED12864::callInteger(const quint8 &row,const quint8 &col,const qint32 &i)
{
    callAction( QLatin1String("integer"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,i);

}
void WioGroveOLED12864::callInverseDisplay(const quint8 &inverse_or_not)
{
    callAction( QLatin1String("inverse_display"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },inverse_or_not);

}
void WioGroveOLED12864::callRotateDisplay(const quint8 &rotate_or_not)
{
    callAction( QLatin1String("rotate_display"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },rotate_or_not);

}
void WioGroveOLED12864::callScrollLeft(const quint8 &start_row,const quint8 &end_row,const quint8 &speed)
{
    callAction( QLatin1String("scroll_left"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },start_row,end_row,speed);

}
void WioGroveOLED12864::callScrollRight(const quint8 &start_row,const quint8 &end_row,const quint8 &speed)
{
    callAction( QLatin1String("scroll_right"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },start_row,end_row,speed);

}
void WioGroveOLED12864::callStopScroll()
{
    callAction( QLatin1String("stop_scroll"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    });

}
void WioGroveOLED12864::callString(const quint8 &row,const quint8 &col,const QString &str)
{
    callAction( QLatin1String("string"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    },row,col,str);

}

QString WioGroveOLED12864::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveOLED12864%1")).arg(pin);
}

