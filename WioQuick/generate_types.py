#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

  Copyright (C) 2016 Ian Reinhart Geiser

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

'''

import optparse
import simplejson
import pystache
import os
import sys

header_template = """/* GENERATED FILE DO NOT EDIT */

/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIO{{ClassName_Upper}}_H
#define WIO{{ClassName_Upper}}_H

#include <QObject>
#include <QVariant>
#include "wiogrovemodule.h"

class Wio{{ClassName}} : public WioGroveModule
{
    Q_OBJECT

{{#ReadOnlyProperties}}    Q_PROPERTY({{ReturnType}} {{NameCamel}} READ {{Name}} NOTIFY {{NameCamel}}Changed)
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}    Q_PROPERTY({{ReturnType}} {{NameCamel}} READ {{Name}} WRITE set_{{Name}} NOTIFY {{NameCamel}}Changed)
{{/ReadWriteProperties}}

public:
    Wio{{ClassName}}( QObject *parent = 0);

{{#ReadOnlyProperties}}    {{ReturnType}} {{Name}}() const;
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}    {{ReturnType}} {{Name}}() const;
    void set_{{Name}}( const {{ReturnType}} &val );
{{/ReadWriteProperties}}

Q_SIGNALS:
{{#ReadOnlyProperties}}    void {{NameCamel}}Changed();
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}    void {{NameCamel}}Changed();
{{/ReadWriteProperties}}

{{#Events}}    void {{Name}}(const {{Type}} &value);
{{/Events}}

public Q_SLOTS:
    virtual void update();
{{#Actions}}    void call{{NameTitle}}({{{ArgumentString}}});
{{/Actions}}

protected:
    virtual QString buildSensorName( const QString &pin ) const;
{{#HasEvents}}    virtual bool emitEvent( const QString &event, const QVariant &data );
    virtual bool hasEvents() const;
{{/HasEvents}}
private:
{{#ReadOnlyProperties}}    {{ReturnType}} m_{{Name}};
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}    {{ReturnType}} m_{{Name}};
{{/ReadWriteProperties}}

};

#endif // WIO{{ClassName_Upper}}_H

"""

cpp_template = """/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "{{HeaderName}}"
#include <QJsonObject>

Wio{{ClassName}}::Wio{{ClassName}}( QObject *parent) : WioGroveModule(parent)
{
{{#ReadOnlyProperties}}     {{InitString}}
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}     {{InitString}}
{{/ReadWriteProperties}}

}

void Wio{{ClassName}}::update()
{

{{#ReadOnlyProperties}}
    readProperty( QLatin1String("{{Name}}"), [&](const QJsonObject &object){
    {{#ReturnsOne}}
       m_{{Name}} = object.toVariantMap()[QLatin1String("{{TypeName}}")].value<{{Type}}>();
    {{/ReturnsOne}}
    {{#ReturnsMany}}
       m_{{Name}}[QLatin1String("{{TypeName}}")] = object.toVariantMap()[QLatin1String("{{TypeName}}")].value<{{Type}}>();
    {{/ReturnsMany}}

       Q_EMIT {{NameCamel}}Changed();
    });
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}
    readProperty( QLatin1String("{{Name}}"), [&](const QJsonObject &object){
    {{#ReturnsOne}}
       m_{{Name}} = object.toVariantMap()[QLatin1String("{{TypeName}}")].value<{{Type}}>();
    {{/ReturnsOne}}
    {{#ReturnsMany}}
       m_{{Name}}[QLatin1String("{{TypeName}}")] = object.toVariantMap()[QLatin1String("{{TypeName}}")].value<{{Type}}>();
    {{/ReturnsMany}}

       Q_EMIT {{NameCamel}}Changed();
    });
{{/ReadWriteProperties}}
}

{{#ReadOnlyProperties}}{{ReturnType}} Wio{{ClassName}}::{{Name}}() const
{
    return m_{{Name}};
}

{{/ReadOnlyProperties}}

{{#ReadWriteProperties}}{{ReturnType}} Wio{{ClassName}}::{{Name}}() const
{
    return m_{{Name}};
}

void Wio{{ClassName}}::set_{{Name}}( const {{ReturnType}} &val )
{

    if ( m_{{Name}} != val ) {
        writeProperty( QLatin1String("{{Name}}"), [&](const QJsonObject &object){
            if ( checkError(object) ) {

            }
            update();
        }{{#ReturnsOne}},val{{/ReturnsOne}}{{#ReturnsMany}},val[QLatin1String("{{TypeName}}")]{{/ReturnsMany}});
    }

}

{{/ReadWriteProperties}}

{{#Actions}}void Wio{{ClassName}}::call{{NameTitle}}({{{ArgumentString}}})
{
    callAction( QLatin1String("{{Name}}"), [&](const QJsonObject &object){
        if ( checkError(object) ) {

        }
    }{{#ArgumentListString}},{{/ArgumentListString}}{{ArgumentListString}});

}
{{/Actions}}

QString Wio{{ClassName}}::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("{{ClassName}}%1")).arg(pin);
}

{{#HasEvents}}
bool Wio{{ClassName}}::emitEvent( const QString &event, const QVariant &data )
{
    {{#Events}}
    if ( event == QLatin1String("{{Event}}") ) {
        Q_EMIT {{Name}}(data.value<{{Type}}>());
        return true;
    }
    {{/Events}}
    return false;
}

bool Wio{{ClassName}}::hasEvents( ) const
{
    return true;
}
{{/HasEvents}}
"""

plugin_header_template = """/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIO_QUICK_PLUGIN_H
#define WIO_QUICK_PLUGIN_H

#include <QQmlExtensionPlugin>

class WioQuickPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif // WIO_QUICK_PLUGIN_H

"""

plugin_cpp_template = """/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "wio_quick_plugin.h"
#include "wionode.h"

{{#Headers}}#include "{{Header}}"
{{/Headers}}

#include <qqml.h>

void WioQuickPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<WioNode>(uri, 1, 0, "WioNode");
    qmlRegisterUncreatableType<WioGroveModule>(uri, 1, 0, "GroveModule", QLatin1String("Use a specific Grove module"));

{{#Types}}    qmlRegisterType<Wio{{Type}}>(uri, 1, 0, "{{Type}}");
{{/Types}}

}


"""

document_template = """# {{ClassName}} Class

## {{{GroveName}}}

![preview]({{{ImageUrl}}})

[Grove Wiki]({{{WikiUrl}}})

{{{Description}}}

{{#HasProperties}}
### Properties

Property | Type | Permissions | Notes
---------|------|-------------|------
{{#ReadOnlyProperties}}**{{NameCamel}}** | {{ReturnType}} | Read | {{{BriefString}}}
{{/ReadOnlyProperties}}
{{#ReadWriteProperties}}**{{NameCamel}}** | {{ReturnType}} | Read, Write | {{{BriefString}}}
{{/ReadWriteProperties}}
{{/HasProperties}}

{{#HasActions}}### Methods

Method | Arguments |Notes
-------|-----------|-----
{{#Actions}}**call{{NameTitle}}** | {{{CleanArgumentString}}} | {{{BriefString}}}
{{/Actions}}
{{/HasActions}}

{{#HasEvents}}### Signals

Signal | Argument | Notes
-------|----------|------
{{#Events}}**{{Name}}** | {{Type}} value | {{{BriefString}}}
{{/Events}}
{{/HasEvents}}

"""

api_index_template = """
# API Documentation Index

* [WioNode](wionode)
{{#TypeIndex}}* [{{ClassName}}]({{FileName}}) - {{{GroveName}}} Instance
{{/TypeIndex}}

"""

def cleanupDocString(str):
    return " ".join(str.split()).replace("<br>"," ").replace("\r"," ").replace("\n", " ")

def camelCaseString(str):
    output = ''.join(x for x in str.title() if x.isalpha() or x.isdigit())
    return output[0].lower() + output[1:]

def stip_prefix( str ):
    return str.replace("write_","").replace("read_","");

def isReadOnly( key, readMembers, writeMembers ):
    if key in readMembers and key not in writeMembers:
        return True
    return False

def isReadWrite( key, readMembers, writeMembers ):
    if key in readMembers and key in writeMembers:
        return True
    return False

def isWriteOnly( key, readMembers, writeMembers ):
    if key not in readMembers and key in writeMembers:
        return True
    return False

def fixupTypes( type ):
    if ( type == "uint8_t" ):
        return "quint8"
    if ( type == "uint16_t" ):
        return "quint16"
    if ( type == "uint32_t" ):
        return "quint32"
    if ( type == "int32_t" ):
        return "qint32"
    if ( type == "int16_t" ):
        return "qint16"

    if ( type == "char *" ):
        return "QString"

    return type

def isPod( type ):
    if type in ["quint8","quint16","quint32","qint32","float","int","bool"]:
        return True

    return False

class TypeDefinitionMember(object):
    def __init__(self, name, returns, arguments):
        self._name = name
        self._returns = returns
        self._arguments = arguments
        self.docs = {}

    def Name(self):
        return self._name

    def NameCamel(self):
        return camelCaseString(self._name)

    def NameTitle(self):
        return self._name.title().replace("_","")

    def ReturnType(self):
        # if there is 1 return set type
        # if there is more than 1 return set QVariantMap
        if len( self._returns) == 1:
           return self._returns[0]["Type"]
        return "QVariantMap"

    def ReturnsOne(self):
        if len( self._returns) == 1:
           return [self._returns[0]]
        return []

    def ReturnsMany(self):
        if len( self._returns) != 1:
           return self._returns
        return []


    def ArgumentString(self):
        return ",".join([
                           "const " + x["Type"] + " &" + x["TypeName"] for x in self._arguments
                        ])

    def CleanArgumentString(self):
        return ",".join([
                           x["Type"] + " " + x["TypeName"] for x in self._arguments
                        ])

    def ArgumentListString(self):
        return ",".join([
                           x["TypeName"] for x in self._arguments
                        ])

    def InitString(self):
        if isPod(self.ReturnType()):
            return "m_" + self.Name() + " = 0;"
        return ""

    def BriefString(self):
        return cleanupDocString(self.docs.get("@brief@",""))

class TypeDefinition(object):
    def __init__(self, moduleData):
        self._moduleData = moduleData
        self._moduleDocs = {}

        writeMembers = [ stip_prefix(x) for x in self._moduleData["Writes"].keys() ]
        readMembers = [ stip_prefix(x) for x in self._moduleData["Reads"].keys() ]

        writeMembers.sort()
        readMembers.sort()

        readOnlyProperies = [ x for x in readMembers if isReadOnly(x,readMembers,writeMembers) ]
        readWriteProperies = [ x for x in writeMembers if isReadWrite(x,readMembers,writeMembers) ]
        actions = [ x for x in writeMembers if isWriteOnly(x,readMembers,writeMembers) ]

        self._readOnlyProperies = [
            TypeDefinitionMember(
                x,
                [ { "Type" : fixupTypes(y[0]), "TypeName" : y[1] } for y in self._moduleData["Reads"]["read_" + x]["Returns"] ],
                []
                ) for x in readOnlyProperies
        ]

        self._readWriteProperies = [
            TypeDefinitionMember(
                x,
                [ { "Type" : fixupTypes(y[0]), "TypeName" : y[1] } for y in self._moduleData["Reads"]["read_" + x]["Returns"] ],
                [ { "Type" : fixupTypes(y[0]), "TypeName" : y[1] } for y in self._moduleData["Writes"]["write_" + x]["Arguments"] ]
            ) for x in readWriteProperies
        ]

        self._actions = [
            TypeDefinitionMember(
                x,
                [],
                [ { "Type" : fixupTypes(y[0]), "TypeName" : y[1] } for y in self._moduleData["Writes"]["write_" + x]["Arguments"] ]
                ) for x in actions
        ]

        self._events = [
                        {
                            "Name": camelCaseString("event_" + x),
                            "Event": x,
                            "Type": fixupTypes(y),
                            "BriefString" : "",
                        } for x,y in sorted(self._moduleData["Events"].items())
                       ]


    def populateModuleDocs(self,obj):
        self._moduleDocs = obj
        for x in self._readOnlyProperies:
            if "read_" + x.Name() in obj["Methods"].keys():
                x.docs = obj["Methods"]["read_" + x.Name()]

        for x in self._readWriteProperies:
            if "write_" + x.Name() in obj["Methods"].keys():
                x.docs = obj["Methods"]["write_" + x.Name()]

        for x in self._actions:
            if "write_" + x.Name() in obj["Methods"].keys():
                x.docs = obj["Methods"]["write_" + x.Name()]

        for x in self._events:
            if x["Event"] in obj["Events"].keys():
                x["BriefString"] = cleanupDocString(obj["Events"][x["Event"]])

    def filename(self,suffix):
        return "generated_" + self._moduleData["ClassName"].lower() + "." + suffix

    def id(self):
        return self._moduleData["ID"]

    def ImageUrl(self):
        return self._moduleData["ImageURL"]

    def WikiUrl(self):
        return self._moduleData["WikiURL"]

    def FileName(self):
        return self._moduleData["ClassName"].lower()

    def HeaderName(self):
        return self.filename("h")

    def ClassName_Upper(self):
        return self._moduleData["ClassName"].upper()

    def ClassName(self):
        return self._moduleData["ClassName"]

    def ReadOnlyProperties(self):
        return self._readOnlyProperies

    def ReadWriteProperties(self):
        return self._readWriteProperies

    def Actions(self):
        return self._actions

    def Events(self):
        return self._events

    def Description(self):
        return self._moduleData["Description"]

    def GroveName(self):
        return self._moduleData["GroveName"]

    def HasActions(self):
        return len(self._actions) != 0

    def HasEvents(self):
        return len(self._events) != 0

    def HasProperties(self):
        return len(self._readOnlyProperies) != 0 or len(self._readWriteProperies) != 0

class TypeDefinitions(object):
    def __init__(self, jsonFile):
        json = simplejson.load(jsonFile)
        self.types = [TypeDefinition(type) for type in json]

    def Headers(self):
        return [ { "Header" : x.filename("h") } for x in self.types ]

    def Types(self):
        return [ { "Type" : x.ClassName() } for x in self.types ]

    def TypeIndex(self):
        return [ {
                    "ClassName" : x.ClassName(),
                    "GroveName" : x.GroveName(),
                    "FileName" : x.FileName()
                 } for x in self.types ]

    def typeByID(self, id):
        for x in self.types:
            if x.id() == id:
                return x
        return None

parser = optparse.OptionParser(usage="usage: %prog [options] drivers.json",version="%prog 1.0")
parser.add_option("-d", "--docs",
                      dest="gen_docs",
                      nargs=1,
                      help="Generate markdown documentation for API")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("wrong number of arguments")

drivers_json = open(args[0])

defs = TypeDefinitions(drivers_json)

renderer = pystache.Renderer()

pri_out = open("generated.pri", "w")
plugin_header_out = open("wio_quick_plugin.h", "w")
plugin_cpp_out = open("wio_quick_plugin.cpp", "w")

if options.gen_docs:
    docs_json = simplejson.load(open(options.gen_docs))
    for x in docs_json:
        type = defs.typeByID(x["ID"])
        type.populateModuleDocs(x)
    api_index_out = open("apidocs.md", "w")
    api_index_out.write(renderer.render(api_index_template, defs))
    api_index_out.close()

pri_out.write("# GENERATED FILE DO NOT EDIT\n")

for module in defs.types:

    hpp_out = open(module.filename("h"), "w")
    hpp_out.write(renderer.render(header_template, module))
    hpp_out.close()
    pri_out.write("HEADERS += " + module.filename("h") + "\n")

    cpp_out = open(module.filename("cpp"), "w")
    cpp_out.write(renderer.render(cpp_template, module))
    cpp_out.close()
    pri_out.write("SOURCES += " + module.filename("cpp") + "\n")

    if options.gen_docs:
        docs_out = open(module.ClassName().lower() + ".md", "w")
        docs_out.write(renderer.render(document_template, module))
        docs_out.close()

plugin_header_out.write(renderer.render(plugin_header_template, defs))
plugin_header_out.close()
pri_out.write("HEADERS += wio_quick_plugin.h\n")

plugin_cpp_out.write(renderer.render(plugin_cpp_template, defs))
plugin_cpp_out.close()
pri_out.write("SOURCES += wio_quick_plugin.cpp\n")

pri_out.close()
