/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "wio_quick_plugin.h"
#include "wionode.h"

#include "generated_groverelay.h"
#include "generated_grovedryreedrelay.h"
#include "generated_genericdin.h"
#include "generated_groveultraranger.h"
#include "generated_grovebarobmp085.h"
#include "generated_grovedust.h"
#include "generated_grovegesture.h"
#include "generated_grovetemphumpro.h"
#include "generated_groveaccmma7660.h"
#include "generated_grovespdtrelay30a.h"
#include "generated_grovemp3v2.h"
#include "generated_groveledws2812.h"
#include "generated_grovetemphum.h"
#include "generated_grovevoldivider.h"
#include "generated_grovebutton.h"
#include "generated_groveairquality.h"
#include "generated_groveirdistanceinterrupter.h"
#include "generated_cytronmd13s.h"
#include "generated_groveencoder.h"
#include "generated_groveexample.h"
#include "generated_genericpwmout.h"
#include "generated_groverotaryangle.h"
#include "generated_grovecompass.h"
#include "generated_groveuv.h"
#include "generated_grovespeaker.h"
#include "generated_grovebme280.h"
#include "generated_grovepirmotion.h"
#include "generated_grovesound.h"
#include "generated_grovesi114x.h"
#include "generated_grovemagneticswitch.h"
#include "generated_genericain.h"
#include "generated_groveluminance.h"
#include "generated_grovesolidstaterelay.h"
#include "generated_groveledbar.h"
#include "generated_grovei2cmotordriver.h"
#include "generated_grovei2cadc.h"
#include "generated_groveirrecv.h"
#include "generated_groveoled9696.h"
#include "generated_grovetemp.h"
#include "generated_grovedigitallight.h"
#include "generated_groveelecmagnet.h"
#include "generated_groveel.h"
#include "generated_grovetemp1wire.h"
#include "generated_grovehall.h"
#include "generated_grovei2cfmreceiver.h"
#include "generated_grovelcdrgb.h"
#include "generated_groveco2mhz16.h"
#include "generated_groveservo.h"
#include "generated_groveoled12864.h"
#include "generated_grove4digit.h"
#include "generated_groverecorder.h"
#include "generated_groveiremit.h"
#include "generated_groveloudness.h"
#include "generated_grovemoisture.h"
#include "generated_groveuart.h"
#include "generated_grovegyroitg3200.h"
#include "generated_genericdout.h"
#include "generated_grovemultichannelgas.h"
#include "generated_grovebarobmp280.h"

#include <qqml.h>

void WioQuickPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<WioNode>(uri, 1, 0, "WioNode");
    qmlRegisterUncreatableType<WioGroveModule>(uri, 1, 0, "GroveModule", QLatin1String("Use a specific Grove module"));

    qmlRegisterType<WioGroveRelay>(uri, 1, 0, "GroveRelay");
    qmlRegisterType<WioGroveDryReedRelay>(uri, 1, 0, "GroveDryReedRelay");
    qmlRegisterType<WioGenericDIn>(uri, 1, 0, "GenericDIn");
    qmlRegisterType<WioGroveUltraRanger>(uri, 1, 0, "GroveUltraRanger");
    qmlRegisterType<WioGroveBaroBMP085>(uri, 1, 0, "GroveBaroBMP085");
    qmlRegisterType<WioGroveDust>(uri, 1, 0, "GroveDust");
    qmlRegisterType<WioGroveGesture>(uri, 1, 0, "GroveGesture");
    qmlRegisterType<WioGroveTempHumPro>(uri, 1, 0, "GroveTempHumPro");
    qmlRegisterType<WioGroveAccMMA7660>(uri, 1, 0, "GroveAccMMA7660");
    qmlRegisterType<WioGroveSPDTRelay30A>(uri, 1, 0, "GroveSPDTRelay30A");
    qmlRegisterType<WioGroveMP3V2>(uri, 1, 0, "GroveMP3V2");
    qmlRegisterType<WioGroveLedWs2812>(uri, 1, 0, "GroveLedWs2812");
    qmlRegisterType<WioGroveTempHum>(uri, 1, 0, "GroveTempHum");
    qmlRegisterType<WioGroveVolDivider>(uri, 1, 0, "GroveVolDivider");
    qmlRegisterType<WioGroveButton>(uri, 1, 0, "GroveButton");
    qmlRegisterType<WioGroveAirquality>(uri, 1, 0, "GroveAirquality");
    qmlRegisterType<WioGroveIRDistanceInterrupter>(uri, 1, 0, "GroveIRDistanceInterrupter");
    qmlRegisterType<WioCytronMD13S>(uri, 1, 0, "CytronMD13S");
    qmlRegisterType<WioGroveEncoder>(uri, 1, 0, "GroveEncoder");
    qmlRegisterType<WioGroveExample>(uri, 1, 0, "GroveExample");
    qmlRegisterType<WioGenericPWMOut>(uri, 1, 0, "GenericPWMOut");
    qmlRegisterType<WioGroveRotaryAngle>(uri, 1, 0, "GroveRotaryAngle");
    qmlRegisterType<WioGroveCompass>(uri, 1, 0, "GroveCompass");
    qmlRegisterType<WioGroveUV>(uri, 1, 0, "GroveUV");
    qmlRegisterType<WioGroveSpeaker>(uri, 1, 0, "GroveSpeaker");
    qmlRegisterType<WioGroveBME280>(uri, 1, 0, "GroveBME280");
    qmlRegisterType<WioGrovePIRMotion>(uri, 1, 0, "GrovePIRMotion");
    qmlRegisterType<WioGroveSound>(uri, 1, 0, "GroveSound");
    qmlRegisterType<WioGroveSI114X>(uri, 1, 0, "GroveSI114X");
    qmlRegisterType<WioGroveMagneticSwitch>(uri, 1, 0, "GroveMagneticSwitch");
    qmlRegisterType<WioGenericAIn>(uri, 1, 0, "GenericAIn");
    qmlRegisterType<WioGroveLuminance>(uri, 1, 0, "GroveLuminance");
    qmlRegisterType<WioGroveSolidStateRelay>(uri, 1, 0, "GroveSolidStateRelay");
    qmlRegisterType<WioGroveLEDBar>(uri, 1, 0, "GroveLEDBar");
    qmlRegisterType<WioGroveI2CMotorDriver>(uri, 1, 0, "GroveI2CMotorDriver");
    qmlRegisterType<WioGroveI2CAdc>(uri, 1, 0, "GroveI2CAdc");
    qmlRegisterType<WioGroveIRRecv>(uri, 1, 0, "GroveIRRecv");
    qmlRegisterType<WioGroveOLED9696>(uri, 1, 0, "GroveOLED9696");
    qmlRegisterType<WioGroveTemp>(uri, 1, 0, "GroveTemp");
    qmlRegisterType<WioGroveDigitalLight>(uri, 1, 0, "GroveDigitalLight");
    qmlRegisterType<WioGroveElecMagnet>(uri, 1, 0, "GroveElecMagnet");
    qmlRegisterType<WioGroveEL>(uri, 1, 0, "GroveEL");
    qmlRegisterType<WioGroveTemp1Wire>(uri, 1, 0, "GroveTemp1Wire");
    qmlRegisterType<WioGroveHall>(uri, 1, 0, "GroveHall");
    qmlRegisterType<WioGroveI2cFmReceiver>(uri, 1, 0, "GroveI2cFmReceiver");
    qmlRegisterType<WioGroveLCDRGB>(uri, 1, 0, "GroveLCDRGB");
    qmlRegisterType<WioGroveCo2MhZ16>(uri, 1, 0, "GroveCo2MhZ16");
    qmlRegisterType<WioGroveServo>(uri, 1, 0, "GroveServo");
    qmlRegisterType<WioGroveOLED12864>(uri, 1, 0, "GroveOLED12864");
    qmlRegisterType<WioGrove4Digit>(uri, 1, 0, "Grove4Digit");
    qmlRegisterType<WioGroveRecorder>(uri, 1, 0, "GroveRecorder");
    qmlRegisterType<WioGroveIREmit>(uri, 1, 0, "GroveIREmit");
    qmlRegisterType<WioGroveLoudness>(uri, 1, 0, "GroveLoudness");
    qmlRegisterType<WioGroveMoisture>(uri, 1, 0, "GroveMoisture");
    qmlRegisterType<WioGroveUART>(uri, 1, 0, "GroveUART");
    qmlRegisterType<WioGroveGyroITG3200>(uri, 1, 0, "GroveGyroITG3200");
    qmlRegisterType<WioGenericDOut>(uri, 1, 0, "GenericDOut");
    qmlRegisterType<WioGroveMultiChannelGas>(uri, 1, 0, "GroveMultiChannelGas");
    qmlRegisterType<WioGroveBaroBMP280>(uri, 1, 0, "GroveBaroBMP280");

}


