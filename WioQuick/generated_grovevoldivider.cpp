/* GENERATED FILE DO NOT EDIT */
/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generated_grovevoldivider.h"
#include <QJsonObject>

WioGroveVolDivider::WioGroveVolDivider( QObject *parent) : WioGroveModule(parent)
{
     m_voltage_x10 = 0;
     m_voltage_x3 = 0;

}

void WioGroveVolDivider::update()
{

    readProperty( QLatin1String("voltage_x10"), [&](const QJsonObject &object){
       m_voltage_x10 = object.toVariantMap()[QLatin1String("volt")].value<float>();

       Q_EMIT voltageX10Changed();
    });
    readProperty( QLatin1String("voltage_x3"), [&](const QJsonObject &object){
       m_voltage_x3 = object.toVariantMap()[QLatin1String("volt")].value<float>();

       Q_EMIT voltageX3Changed();
    });
}

float WioGroveVolDivider::voltage_x10() const
{
    return m_voltage_x10;
}

float WioGroveVolDivider::voltage_x3() const
{
    return m_voltage_x3;
}




QString WioGroveVolDivider::buildSensorName( const QString &pin ) const
{
    return QString(QLatin1String("GroveVolDivider%1")).arg(pin);
}

