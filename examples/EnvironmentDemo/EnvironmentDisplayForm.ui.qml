import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 400
    height: 400
    property alias temperatureMeter: temperatureMeter
    property alias historyChart: historyChart
    property alias humidityMeter: humidityMeter
    property alias lightMeter: lightMeter
    property alias airQualityMeter: airQualityMeter
    property alias settingsButton: settingsButton

    GridLayout {
        id: grid1
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: parent.top
        anchors.topMargin: 14
        columns: 3

        Label {
            id: label1
            text: qsTr("Air Quality")
        }

        ProgressBar {
            id: airQualityMeter
            Layout.fillWidth: true
        }

        Label {
            id: airQualityText
            text: airQualityMeter.value
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: label2
            text: qsTr("Light")
        }

        ProgressBar {
            id: lightMeter
            Layout.fillWidth: true
        }

        Label {
            id: lightMeterText
            text: lightMeter.value
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: label3
            text: qsTr("Relative Humidity")
        }

        ProgressBar {
            id: humidityMeter
            Layout.fillWidth: true
        }

        Label {
            id: humidityText
            text: humidityMeter.value
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: label5
            text: qsTr("Temperature")
        }

        ProgressBar {
            id: temperatureMeter
            Layout.fillWidth: true
        }

        Label {
            id: temperatureText
            text: temperatureMeter.value
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

    }

    Item {
        id: historyChart
        anchors.bottom: settingsButton.top
        anchors.bottomMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: grid1.bottom
        anchors.topMargin: 15
    }

    Button {
        id: settingsButton
        text: qsTr("Configure")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
    }

}
