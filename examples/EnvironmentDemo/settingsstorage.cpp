#include "settingsstorage.h"
#include <QSettings>

SettingsStorage::SettingsStorage(QObject *parent) : QObject(parent)
{

}

QString SettingsStorage::serverAddress() const
{
    QSettings settings;
    return settings.value(QLatin1String("Server/Address")).value<QString>();
}

void SettingsStorage::setServerAddress(const QString &value)
{
    if ( serverAddress() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("Server/Address"), value );
        Q_EMIT serverAddressChanged();
    }
}

QString SettingsStorage::nodeName() const
{
    QSettings settings;
    return settings.value(QLatin1String("Node/Name")).value<QString>();
}

void SettingsStorage::setNodeName(const QString &value)
{
    if ( nodeName() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("Node/Name"), value );
        Q_EMIT nodeNameChanged();
    }
}

QString SettingsStorage::authKey() const
{
    QSettings settings;
    return settings.value(QLatin1String("User/AuthKey")).value<QString>();
}

void SettingsStorage::setAuthKey(const QString &value)
{
    if ( authKey() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("User/AuthKey"), value );
        Q_EMIT authKeyChanged();
    }
}
