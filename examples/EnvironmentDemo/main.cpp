#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "settingsstorage.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(QLatin1String("EnvironmentDemo"));
    app.setOrganizationName(QLatin1String("Geek Central"));
    app.setOrganizationDomain(QLatin1String("geekcentral.pub"));

    SettingsStorage settings;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("AppSettings", &settings);
    engine.addImportPath(QLatin1String("../../WioQuick"));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
