#ifndef SETTINGSSTORAGE_H
#define SETTINGSSTORAGE_H

#include <QObject>

class SettingsStorage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString serverAddress READ serverAddress WRITE setServerAddress NOTIFY serverAddressChanged)
    Q_PROPERTY(QString nodeName READ nodeName WRITE setNodeName NOTIFY nodeNameChanged)
    Q_PROPERTY(QString authKey READ authKey WRITE setAuthKey NOTIFY authKeyChanged)

public:
    explicit SettingsStorage(QObject *parent = 0);

    QString serverAddress() const;
    void setServerAddress(const QString &value);

    QString nodeName() const;
    void setNodeName(const QString &value);

    QString authKey() const;
    void setAuthKey(const QString &value);

signals:
    void serverAddressChanged();
    void nodeNameChanged();
    void authKeyChanged();

public slots:

};

#endif // SETTINGSSTORAGE_H
