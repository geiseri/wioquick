import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import Seeed.Wio 1.0

ApplicationWindow {
    visible: true
    width: 320
    height: 240
    title: qsTr("Wio Link Environment Kit Demo")

    WioNode {
        id: node
        name: config.nodeName.text
        dataExchangeServer: config.serverAddress.text
        userToken: config.authToken.text

        onLastErrorChanged: console.log(node.lastError)

        GroveTempHum {
            id: tempNhum
            pin: "D0"
        }

        GroveDigitalLight {
            id: light
            pin: "I2C0"
        }

        GroveAirquality {
            id: airQuality
            pin: "A0"
        }

        onOnlineChanged: {
            if ( node.online ) {
                poller.start();
            } else {
                poller.stop();
            }
        }
    }

    Timer {
        id: poller
        interval: 30000
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            node.update();
            tempNhum.update();
            light.update();
            airQuality.update();
        }
    }

    StackView {
        id: mainStack
        anchors.fill: parent
        initialItem: display

        EnvironmentDisplay {
            id: display
            settingsButton.onClicked: {
                poller.stop();
                mainStack.push(config);
            }

            humidityMeter.value: tempNhum.humidity
            airQualityMeter.value: airQuality.quality
            temperatureMeter.value: tempNhum.temperature
            lightMeter.value: light.lux
        }

        Configuration {
            id: config
            visible: false
            saveSettings.onClicked: {
                mainStack.pop();
                poller.start();
            }
            cancelButton.onClicked: {
                restore();
                mainStack.pop();
                poller.start();
            }
        }
    }
}


