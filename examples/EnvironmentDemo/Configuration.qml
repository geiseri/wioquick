import QtQuick 2.4

ConfigurationForm {

    saveSettings.onClicked: {
        AppSettings.authKey = authToken.text
        AppSettings.nodeName = nodeName.text
        AppSettings.serverAddress = serverAddress.text
    }

    Component.onCompleted: {
        restore();
    }

    function restore() {
        authToken.text = AppSettings.authKey;
        nodeName.text = AppSettings.nodeName;
        serverAddress.text = AppSettings.serverAddress
    }
}
