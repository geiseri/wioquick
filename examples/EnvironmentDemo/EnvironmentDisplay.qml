import QtQuick 2.4

EnvironmentDisplayForm {
    humidityMeter.minimumValue: 0
    humidityMeter.maximumValue: 100
    temperatureMeter.minimumValue: -50
    temperatureMeter.maximumValue: 50
    airQualityMeter.minimumValue: 0
    airQualityMeter.maximumValue: 100
    lightMeter.minimumValue: 0
    lightMeter.maximumValue: 1000
}
