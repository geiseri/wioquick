import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

Item {
    id: configuration
    width: 400
    height: 400
    property alias cancelButton: cancelButton
    property alias saveSettings: saveSettings
    property alias nodeName: nodeName
    property alias authToken: authToken
    property alias serverAddress: serverAddress

    GroupBox {
        id: groupBox1
        anchors.rightMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.topMargin: 15
        anchors.fill: parent
        checked: false
        title: qsTr("Settings")

        GridLayout {
            id: gridLayout2
            columns: 2
            anchors.fill: parent


            Label {
                id: label1
                text: qsTr("Server Address")
            }

            TextField {
                id: serverAddress
                text: ""
                placeholderText: "us.wio.seeed.io"
                Layout.fillWidth: true
            }

            Label {
                id: label2
                text: qsTr("User Auth Token")
            }

            TextField {
                id: authToken
                Layout.fillWidth: true
                placeholderText: qsTr("")
            }

            Label {
                id: label3
                text: qsTr("Node Name")
            }

            TextField {
                id: nodeName
                text: ""
                Layout.fillWidth: true
                placeholderText: qsTr("Environment Node")
            }

            Item {
                id: item1
                width: 200
                height: 200
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.columnSpan: 2
            }

            RowLayout {
                id: rowLayout1
                width: 100
                height: 100
                Layout.rowSpan: 1
                Layout.fillHeight: false
                Layout.fillWidth: true
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                Button {
                    id: saveSettings
                    text: qsTr("Save")
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    isDefault: true
                }

                Button {
                    id: cancelButton
                    text: qsTr("Cancel")
                }
            }


        }
    }
}
