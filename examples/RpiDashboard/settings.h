/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString dataExchangeServer READ dataExchangeServer WRITE setDataExchangeServer NOTIFY dataExchangeServerChanged)
    Q_PROPERTY(QString nodeName READ nodeName WRITE setNodeName NOTIFY nodeNameChanged)
    Q_PROPERTY(QString userToken READ userToken WRITE setUserToken NOTIFY userTokenChanged)
    Q_PROPERTY(bool imperial READ imperial WRITE setImperial NOTIFY imperialChanged)

public:
    explicit Settings(QObject *parent = 0);


    QString dataExchangeServer() const;
    void setDataExchangeServer(const QString &value);

    QString nodeName() const;
    void setNodeName(const QString &value);

    QString userToken() const;
    void setUserToken(const QString &value);

    bool imperial();
    void setImperial(const bool &value);

signals:
    void dataExchangeServerChanged();
    void nodeNameChanged();
    void userTokenChanged();
    void imperialChanged();
};

#endif // SETTINGS_H
