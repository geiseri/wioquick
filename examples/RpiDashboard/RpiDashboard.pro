TEMPLATE = app

QT += qml quick widgets
CONFIG += c++11

SOURCES += main.cpp \
    engine.cpp \
    settings.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = $$PWD/support \
                  $$PWD/vendor \
                  $$OUT_PWD/../../WioQuick

HEADERS += \
    engine.h \
    settings.h
