/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.3
import QtQuick.Controls 1.0

Rectangle {

    width: 360
    height: 360
    color: "#000000"
    property alias configurationPage: configurationPage
    property alias displayStack: displayStack
    property alias environmentPage: environmentPage

    StackView {
        id: displayStack
        anchors.fill: parent
        initialItem: environmentPage

        DashboardPage {
            id: environmentPage
        }

        ConfigurationPage {
            id: configurationPage
            visible: false
        }
    }
}
