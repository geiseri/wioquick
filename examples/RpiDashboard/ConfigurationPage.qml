/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Seeed.Wio 1.0

ConfigurationPageForm {
    id: root

    WioNode {
        id: node
        dataExchangeServer: root.dataExchangeServer
        userToken: root.userToken
        name: root.nodeName
    }
    onVisibleChanged: {
        node.update();
    }

    model: node.model
    serialNumber: node.serialNumber
    saveButton.enabled: node.isValid
}
