/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import jbQuick.Charts 1.0

Item {
    id: root
    Chart {
        id: chart
        anchors.fill: parent
        chartAnimated: true;
        chartAnimationEasing: Easing.InOutElastic
        chartAnimationDuration: 2000
        chartType: Charts.ChartType.LINE
        chartOptions: {
            return {
                scaleFontFamily: "Raleway",
                scaleFontSize: 12,
                scaleFontColor: "#fff",
                scaleOverride: true,
                scaleStartValue: root.ymin,
                scaleStepWidth: 10,
                scaleSteps: (root.ymax - root.ymin) / 10,
            }
        }

        chartData: {
            return {
                labels: root.xdata,
                datasets: [{
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointDot: false,
                        pointDotRadius: 1,
                        pointDotStrokeWidth: 1,
                        data: root.ydata
                    }]
            }
        }

    }
    property var ydata: []
    property var xdata: []
    property int ymin: 0
    property int ymax: 0
}
