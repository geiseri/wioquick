/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.3
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import Keyboard 1.0

Item {
    id: root
    width: 400
    height: 400
    property alias cancelButton: cancelButton
    property alias saveButton: saveButton

    property alias dataExchangeServer: dataExchangeServer.text
    property alias userToken: userToken.text
    property alias nodeName: nodeName.text
    property alias imperialUnits: imperialUnits.checked
    property alias serialNumber: serialNumber.text
    property alias model: model.text

    Rectangle {
        id: configurationRoot
        height: 200
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: parent.top
        anchors.topMargin: 15

        GridLayout {
            id: grid1
            anchors.rightMargin: 15
            anchors.leftMargin: 15
            anchors.bottomMargin: 15
            anchors.topMargin: 15
            anchors.fill: parent
            columns: 2

            Label {
                id: label1
                text: qsTr("Server Address")
                font.pointSize: 12
            }

            TextField {
                id: dataExchangeServer
                Layout.fillWidth: true
                placeholderText: qsTr("Server Address")
            }

            Label {
                id: label2
                text: qsTr("User Token")
                font.pointSize: 12

            }

            TextField {
                id: userToken

                Layout.fillWidth: true
                placeholderText: qsTr("User Token")
            }

            Label {
                id: label3
                text: qsTr("Node Name")
                font.pointSize: 12

            }

            TextField {
                id: nodeName
                Layout.fillWidth: true
                placeholderText: qsTr("Node Name")

            }

            CheckBox {
                id: imperialUnits
                text: qsTr("Imperial Units")
                Layout.fillWidth: true
                Layout.columnSpan: 2
            }

            RowLayout {
                id: rowLayout1
                width: 100
                height: 100
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.columnSpan: 2
                GridLayout {
                    columns: 2
                    Layout.fillWidth: true
                    Layout.fillHeight: true





                    Item {
                        id: item1
                        width: 200
                        height: 200
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.columnSpan: 2
                    }

                    Label {
                        id: label4
                        text: qsTr("Model")
                    }

                    Label {
                        id: model
                        text: qsTr("")
                        Layout.fillWidth: true
                    }

                    Label {
                        id: label6
                        text: qsTr("Serial Number")
                    }

                    Label {
                        id: serialNumber
                        text: qsTr("")
                        Layout.fillWidth: true
                    }


                }
                Button {
                    id: saveButton
                    text: qsTr("Save")
                    Layout.columnSpan: 1
                    Layout.fillHeight: false
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                }

                Button {
                    id: cancelButton
                    text: qsTr("Cancel")
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                }
            }


        }

    }
    VirtualKeyboard {
        id: keyboard
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        anchors.top: configurationRoot.bottom
        anchors.topMargin: 0

    }
}
