/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.3
import QtQuick.Window 2.0
import QtQuick.Controls 1.0

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("RpiDashboard")

    Wio {
        id: wioNode
        nodeName: Settings.nodeName
        dataExchangeServer: Settings.dataExchangeServer
        userToken: Settings.userToken

        Timer {
            id: poller
            interval: 1000 * 60 * 30
            repeat: true
            triggeredOnStart: true
            onTriggered: {
                if( wioNode.online ) {
                    storage.appendMeasurements(wioNode.temperature, wioNode.humidity);
                }
            }
        }

        Component.onCompleted: poller.start();

    }

    Storage {
        id: storage
    }

    MainForm {
        anchors.fill: parent

        environmentPage.configurationButton.onClicked: {
            configurationPage.dataExchangeServer = Settings.dataExchangeServer;
            configurationPage.userToken = Settings.userToken;
            configurationPage.nodeName = Settings.nodeName;
            configurationPage.imperialUnits = Settings.imperial;
            displayStack.push(configurationPage);
        }

        environmentPage.temprature: wioNode.temperature
        environmentPage.humidity: wioNode.humidity

        environmentPage.temperatureGraph.ydata: storage.tempratureHistory
        environmentPage.temperatureGraph.xdata: storage.times

        environmentPage.humidityGraph.ydata: storage.humidityHistory
        environmentPage.humidityGraph.xdata: storage.times

        configurationPage.saveButton.onClicked: {
            Settings.dataExchangeServer = configurationPage.dataExchangeServer;
            Settings.userToken = configurationPage.userToken;
            Settings.nodeName = configurationPage.nodeName;
            Settings.imperial = configurationPage.imperialUnits;
            displayStack.pop();
        }

        configurationPage.cancelButton.onClicked: {
            displayStack.pop();
        }

    }
}
