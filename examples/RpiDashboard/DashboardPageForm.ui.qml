/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.3
import QtQuick.Controls 1.0

Item {
    id: root
    width: 400
    height: 400
    property alias temperatureGraph: temperatureGraph
    property alias humidityGraph: humidityGraph
    property alias humidityDisplay: humidityDisplay
    property alias tempratureDisplay: tempratureDisplay
    property alias configurationButton: configurationButton


    Button {
        id: configurationButton
        x: 179
        y: 129
        text: qsTr("configure")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
    }



    BoldLabel {
        id: tempratureDisplay
        x: 320
        width: 64
        height: 64
        color: "#ffffff"
        anchors.right: temperatureGraph.right
        anchors.rightMargin: 15
        opacity: 0.75
        anchors.top: temperatureGraph.top
        anchors.topMargin: 15
        background: "#000000"
    }

    BoldLabel {
        id: humidityDisplay
        x: 214
        width: 64
        height: 64
        color: "#ffffff"
        anchors.right: humidityGraph.right
        anchors.rightMargin: 15
        opacity: 0.75
        anchors.top: humidityGraph.top
        anchors.topMargin: 15
        background: "#000000"
    }

    HistoryGraph {
        id: humidityGraph
        height: root.height / 3.0
        ymin: 0
        ymax: 100
        anchors.bottom: configurationButton.top
        anchors.bottomMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
    }

    HistoryGraph {
        id: temperatureGraph
        height: root.height / 3.0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: humidityGraph.top
        anchors.bottomMargin: 5
        ymin: -25
        ymax: 25
    }

}
