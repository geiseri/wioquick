/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import Keyboard 1.0

Button {
    id: root

    property int key: Qt.Key_unknown
    property int modifier: Qt.NoModifier
    property Item target: undefined

    checkable: key === Qt.Key_Shift

    onClicked: {
        if (!target)
            return;

        if ( text.length === 1 ) {
            InputEventSource.keyClickChar(text,modifier,-1);
        } else {
            InputEventSource.keyClick(key, modifier, -1);
        }
    }

    style: ButtonStyle {
        label: Text {
            font.family: "Comfortaa"
            text:control.text
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
}

