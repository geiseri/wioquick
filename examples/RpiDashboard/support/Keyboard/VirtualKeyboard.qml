/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import Keyboard 1.0

Item {
    id: root

    function changeCase(key, alt, modifier) {
        if ( modifier === Qt.ShiftModifier ) {
            if( alt ) {
                return String.fromCharCode(alt);
            } else {
                return String.fromCharCode(key).toUpperCase();
            }
        } else {
            return String.fromCharCode(key).toLowerCase();
        }
    }

    property var keyModel: {
        var keys = [];
        keys.push({key:Qt.Key_1, alt:Qt.Key_Exclam});
        keys.push({key:Qt.Key_2, alt:Qt.Key_At});
        keys.push({key:Qt.Key_3, alt:Qt.Key_NumberSign});
        keys.push({key:Qt.Key_4, alt:Qt.Key_Dollar});
        keys.push({key:Qt.Key_5, alt:Qt.Key_Percent});
        keys.push({key:Qt.Key_6});
        keys.push({key:Qt.Key_7, alt:Qt.Key_Ampersand});
        keys.push({key:Qt.Key_8, alt:Qt.Key_Asterisk});
        keys.push({key:Qt.Key_9 });
        keys.push({key:Qt.Key_0});
        keys.push({text:"Back", key:Qt.Key_Backspace});

        keys.push({text: "Shift", key:Qt.Key_Shift});
        keys.push({key:Qt.Key_Q});
        keys.push({key:Qt.Key_W});
        keys.push({key:Qt.Key_E});
        keys.push({key:Qt.Key_R});
        keys.push({key:Qt.Key_T});
        keys.push({key:Qt.Key_Y});
        keys.push({key:Qt.Key_U});
        keys.push({key:Qt.Key_I});
        keys.push({key:Qt.Key_O});
        keys.push({key:Qt.Key_P});


        keys.push({key:Qt.Key_A});
        keys.push({key:Qt.Key_S});
        keys.push({key:Qt.Key_D});
        keys.push({key:Qt.Key_F});
        keys.push({key:Qt.Key_G});
        keys.push({key:Qt.Key_H});
        keys.push({key:Qt.Key_J});
        keys.push({key:Qt.Key_K});
        keys.push({key:Qt.Key_L});
        keys.push({key:Qt.Key_Semicolon, alt:Qt.Key_Colon});
        keys.push({key:Qt.Key_Apostrophe, alt:Qt.Key_QuoteDbl});

        keys.push({key:Qt.Key_Z});
        keys.push({key:Qt.Key_X});
        keys.push({key:Qt.Key_C});
        keys.push({key:Qt.Key_V});
        keys.push({key:Qt.Key_B});
        keys.push({key:Qt.Key_N});
        keys.push({key:Qt.Key_M});
        keys.push({key:Qt.Key_Comma});
        keys.push({key:Qt.Key_Period});
        keys.push({key:Qt.Key_Backslash, alt:Qt.Key_Question});
        keys.push({text:"Space", key:Qt.Key_Space});

        return keys;
    }

    property int currentModifier: Qt.NoModifier
    property Item target: parent

    Grid {
        id: grid
        anchors.fill: parent
        columns: 11
        rows: 4

        Repeater {
            model: keyModel
            delegate: VirtualKey {
                modifier: root.currentModifier
                key: modelData.key
                text: modelData.text ? modelData.text : changeCase(modelData.key,modelData.alt,root.currentModifier)
                target: root.target
                height: grid.height / grid.rows
                width: grid.width / grid.columns

                onCheckedChanged: {
                    if ( modelData.key === Qt.Key_Shift ) {
                        root.currentModifier = checked ? Qt.ShiftModifier : Qt.NoModifier;
                    }
                }
            }
        }
    }
}
