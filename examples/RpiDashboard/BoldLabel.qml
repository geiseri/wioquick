/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.3

Item {
    id: root

    property alias text: textLabel.text
    property alias color: textLabel.color
    property alias background: background.color
    property int textMargin: 10

    Rectangle {
        anchors.fill: parent
        id: background
        Text {
            id: textLabel
            anchors.fill: parent
            anchors.rightMargin: root.textMargin
            anchors.leftMargin: root.textMargin
            anchors.bottomMargin: root.textMargin
            anchors.topMargin: root.textMargin
            font.family: "Comfortaa"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: root.height - (anchors.topMargin + anchors.bottomMargin)
            textFormat: Text.RichText
        }
    }
}
