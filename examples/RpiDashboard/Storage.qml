/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import "qrc:/vendor/quickmodel/quickmodel.js" as QuickModel

Item {

    property var tempratureHistory: []
    property var humidityHistory: []
    property var times: []
    property int history: 12

    function appendMeasurements( temprature, humidity ) {

        var stamp = new Date();

        times.push(Qt.formatTime(stamp, "h:mm ap"));
        if ( times.length > history ) {
            times.shift();
        }

        timesChanged();

        tempratureHistory.push(temprature);
        if ( tempratureHistory.length > history ) {
            tempratureHistory.shift();
        }

        tempratureHistoryChanged();

        humidityHistory.push(humidity);

        if ( humidityHistory.length > history ) {
            humidityHistory.shift();
        }

        humidityHistoryChanged();
    }
}
