/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QSettings>

Settings::Settings(QObject *parent) : QObject(parent)
{

}

QString Settings::dataExchangeServer() const
{
    QSettings settings;
    return settings.value(QLatin1String("dataExchangeServer/Address")).value<QString>();
}

void Settings::setDataExchangeServer(const QString &value)
{
    if ( dataExchangeServer() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("dataExchangeServer/Address"), value );
        Q_EMIT dataExchangeServerChanged();
    }
}

QString Settings::nodeName() const
{
    QSettings settings;
    return settings.value(QLatin1String("Node/Name")).value<QString>();
}

void Settings::setNodeName(const QString &value)
{
    if ( nodeName() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("Node/Name"), value );
        Q_EMIT nodeNameChanged();
    }
}

QString Settings::userToken() const
{
    QSettings settings;
    return settings.value(QLatin1String("User/userToken")).value<QString>();
}

void Settings::setUserToken(const QString &value)
{
    if ( userToken() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("User/userToken"), value );
        Q_EMIT userTokenChanged();
    }
}

bool Settings::imperial()
{
    QSettings settings;
    return settings.value(QLatin1String("Units/Imperial")).value<bool>();
}

void Settings::setImperial(const bool &value)
{
    if ( imperial() != value ) {
        QSettings settings;
        settings.setValue(QLatin1String("Units/Imperial"), value );
        Q_EMIT imperialChanged();
    }
}
