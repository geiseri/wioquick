/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Seeed.Wio 1.0

Item {
    property alias nodeName: node.name
    property alias dataExchangeServer: node.dataExchangeServer
    property alias userToken: node.userToken
    property alias online: node.online
    property alias temperature: tempNhum.temperature
    property alias humidity: tempNhum.humidity
    property alias model: node.model
    property alias serialNumber: node.serialNumber

    WioNode {
        id: node

        GroveTempHum {
            id: tempNhum
            pin: "D0"
        }

        onOnlineChanged: {
            if ( node.online ) {
                poller.start();
            } else {
                poller.stop();
            }
        }
    }

    Timer {
        id: poller
        interval: 30000
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            node.update();
            tempNhum.update();
        }
    }
}
