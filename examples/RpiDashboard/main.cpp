/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QFont>
#include <QFontDatabase>
#include <QFont>

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "engine.h"
#include "settings.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName(QLatin1String("RpiDashboard"));
    app.setOrganizationName(QLatin1String("Geek Central"));
    app.setOrganizationDomain(QLatin1String("geekcentral.pub"));

    QFontDatabase::addApplicationFont(QLatin1String(":/fonts/Comfortaa-Bold.ttf"));
    QFontDatabase::addApplicationFont(QLatin1String(":/fonts/Raleway-Regular.ttf"));

    app.setFont(QFont(QLatin1String("Raleway")));

    QQmlApplicationEngine engine;
    engine.addImportPath(QLatin1String("../../WioQuick"));
    engine.addImportPath(QLatin1String("qrc:/vendor"));
    engine.addImportPath(QLatin1String("qrc:/support"));

    Engine e;
    engine.rootContext()->setContextProperty(QLatin1String("Engine"), &e);

    Settings s;
    engine.rootContext()->setContextProperty(QLatin1String("Settings"), &s);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
