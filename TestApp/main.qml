/*
 * Copyright (C) 2016 Ian Reinhart Geiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Window 2.2
import Seeed.Wio 1.0

Window {
    visible: true
    width: 200
    height: 100
    title: qsTr("Hello World")

    WioNode {
        id: node
        name: "Geek Node"
        dataExchangeServer: "us.wio.seeed.io"
        userToken: "XXX"

        onLastErrorChanged: console.log(node.lastError)

        GroveButton {
            id: sensor
            pin: "D1"
            onEventButtonPressed: {
                console.log("Pressed: %1".arg(value));
            }
        }

        GrovePIRMotion {
            id: pir
            pin: "D0"
            onEventIrMoved: {
                console.log("Moved: %1".arg(value));
                sensor.update();
            }
        }

    }

    TextEdit {
        id: textEdit
        text: "%1 (%2)".arg(node.name).arg(node.model)
        verticalAlignment: Text.AlignVCenter
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        Rectangle {
            anchors.fill: parent
            anchors.margins: -10
            color: "transparent"
            border.width: 1
        }
        visible: node.isValid
    }
}
