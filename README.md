# A QML wrapper around the Wio REST API

License: LGPLv3

# About
This is a QML wrapper around the Wio Link and Wio Node REST interface. See the [wiki](https://bitbucket.org/geiseri/wioquick/wiki/Home) for more information

# Install

## Source

```
$ git clone http://bitbucket.org/geiseri/wioquick.git
$ cd wioquick
$ qmake
$ make
$ make install
```

## Debian / Ubuntu / Raspbian

```
$ apt-get install debhelper qtdeclarative5-dev libqt5websockets5-dev qt5-qmake 
$ git clone http://bitbucket.org/geiseri/wioquick.git
$ cd wioquick
$ dpkg-buildpackage
$ sudo dpkg -i wioquick-*deb
```

## Arch Linux

```
$ git clone http://bitbucket.org/geiseri/wioquick.git
$ cd wioquick
$ makepkg -s
$ sudo pacman -U wioquick*pkg.tar.xz
```

# Links
* API Information: http://seeed-studio.github.io/Wio_Link/

**NOTE** - This project is not written or supported by Seeed Studio.  They just make the awesome service and hardware that this runs on top of.  If you have issues please report them on this site, not theirs!